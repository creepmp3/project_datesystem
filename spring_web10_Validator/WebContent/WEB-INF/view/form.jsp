<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
    <h1>스프링 폼 태그 연습용</h1>
    
    <form:form action="#" name="myForm" cssClass="form" enctype="multipart/form-data" commandName="myForm" method="post">
        <table>
            <tr>
                <th><form:label path="name">name</form:label></th>
	            <td><form:input path="name"/></td>
	        </tr>
	        <tr>
	            <th><form:label path="gender">gender</form:label></th>
		        <td>
		            <form:radiobutton path="gender" value="M" label="남"/>
		            <form:radiobutton path="gender" value="F" label="여"/>
		        </td>
		    </tr>
	        <tr>
	            <th><form:label path="hiddenField">hiddenField</form:label>
	            <td><form:hidden path="hiddenField"/></td>
	        </tr>
	        <tr>
	           <th><form:label path="password">password</form:label></th>
	           <td><form:password path="password"/></td>
	        </tr>
	        <tr>
	           <th><form:label path="select">select</form:label></th>
	           <td>
		           <form:select path="select">
		                <form:option value="0" label="메뉴를 고르세요"/>
		                <form:options items="${menuList }" id="menuno" itemLabel="name"/>
		           </form:select>
	           </td>
	        </tr>
	        <tr>
	            <th><form:label path="checkbox">checkbox</form:label></th>
	            <td><form:checkbox path="checkbox" value="메뉴" label="전체선택"/></td>
	        </tr>
	        <tr>
	            <th><form:label path="textarea">textarea</form:label></th>
	            <td><form:textarea path="textarea" cols="35" rows="3"/></td>
	        </tr>
	        <tr>
	            <th><form:label path="file">file</form:label></th>
	            <td><form:input path="file" type="file"/></td>
	        </tr>
	    </table>
    </form:form>
</body>
</html>