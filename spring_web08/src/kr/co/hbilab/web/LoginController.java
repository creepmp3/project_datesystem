package kr.co.hbilab.web;

import kr.co.hbilab.dao.MemDAO;
import kr.co.hbilab.dto.MemDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 2015. 6. 1.
 * @author KDY
 */

@Controller 
public class LoginController {
    @Autowired
    MemDAO dao;
    
    public void setDao(MemDAO dao) {
        this.dao = dao;
    }
    
    @RequestMapping("/login/login")
    public String login(){
        return "login/login";
    }
    
    @RequestMapping("/login/loginOk")
    public String loginOk(@RequestParam("id") String id, @RequestParam("pw") String pw){
        MemDTO dto = new MemDTO();
        dto.setId(id);
        dto.setPw(pw);
        
        return "login/login";
    }
}
