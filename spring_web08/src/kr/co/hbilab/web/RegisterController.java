package kr.co.hbilab.web;

/**
 * 2015. 6. 1.
 * @author KDY
 */
import java.util.List;

import kr.co.hbilab.dao.MemDAO;
import kr.co.hbilab.dto.MemDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
@Controller
public class RegisterController {
    
    @Autowired
    MemDAO dao;
    
    public void setDao(MemDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value="/register/step1")
    public String processStep1(){
       return "step1"; 
    }
    
    // Get방식 접근제한
    //@RequestMapping(value="/register/step2.do", method=RequestMethod.GET)
    //public String processStep2(HttpServletRequest req){
    // String ck = req.getParameter("ck");
    @RequestMapping(value="/register/step2")
    public String processStep2(@RequestParam(value="ck", defaultValue="false") Boolean check){
        // @RequestParam에 해당하는value값을 지정한 타입으로 바로 담아서 가져온다
        // ck 파라미터가 없으면 기본값으로 false를 지정한다
        //System.out.println("check  : "+ check);
        if(check){
            return "step2";
        }else{
            return "step1";
        }
    }
    
    @RequestMapping(value="/register/step3", method=RequestMethod.POST)
    public String processStep3(
              Model model
            , @RequestParam("id") String id
            , @RequestParam("pw") String pw
            , @RequestParam("email") String email){
        
        MemDTO dto = new MemDTO();
        dto.setId(id);
        dto.setPw(pw);
        dto.setEmail(email);
        
        dao.insertOne(dto);
        model.addAttribute("id", id);
        
        return "welcome";
    }
    
    @RequestMapping(value="List")
    public String selectMemAll(Model model){
        List<MemDTO> list = dao.selectAll();
        model.addAttribute("list", list);
       return "list"; 
    }
    
}
