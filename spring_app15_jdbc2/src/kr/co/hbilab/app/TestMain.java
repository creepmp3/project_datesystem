package kr.co.hbilab.app;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

//20150526
public class TestMain {
    public static void main(String[] args) {
        
        ApplicationContext ctx = new GenericXmlApplicationContext("app.xml");
        Dao dao = ctx.getBean("dao", Dao.class);
        List<EmpDTO> list = dao.selectAll();
        
        System.out.println("사원번호\t사원명\t직업\t\tMgr\t입사일\t\t급여\tComm\t부서번호");
        for(EmpDTO dto : list){
            System.out.print(dto.getEmpno()+"\t");
            System.out.print(dto.getEname()+"\t");
            System.out.print(dto.getJob()+"\t");
            System.out.print(dto.getMgr()+"\t");
            System.out.print(dto.getHiredate()+"\t");
            System.out.print(dto.getSal()+"\t");
            System.out.print(dto.getComm()+"\t");
            System.out.print(dto.getDeptno()+"\n");
        }
        
        System.out.println("--------------------------------------------------------------------------");
        
        EmpDTO dto = dao.selectOne(7788);
        System.out.print(dto.getEmpno()+"\t");
        System.out.print(dto.getEname()+"\t");
        System.out.print(dto.getJob()+"\t");
        System.out.print(dto.getMgr()+"\t");
        System.out.print(dto.getHiredate()+"\t");
        System.out.print(dto.getSal()+"\t");
        System.out.print(dto.getComm()+"\t");
        System.out.print(dto.getDeptno()+"\n");
     
        System.out.println("--------------------------------------------------------------------------");
        /*
        EmpDTO dto2 = new EmpDTO();
        dto2.setEmpno(7777);
        dto2.setEname("SCOTT2");
        dto2.setJob("ANALYST");
        dto2.setMgr(7788);
        dto2.setSal(5000);
        dto2.setComm(500);
        dto2.setDeptno(40);
        
        dao.insertOne(dto2);
        */
        
        /*
        EmpDTO dto2 = new EmpDTO();
        dto2.setEmpno(0);
        dto2.setEname("SCOTT3");
        dto2.setJob("ANALYST");
        dto2.setMgr(7788);
        dto2.setSal(5777);
        dto2.setComm(777);
        dto2.setDeptno(30);
        
        dao.updateOne(dto2);
        
        */
        /*
        dao.deleteOne(0);
        */
    }
}
