/**
 * 2015. 6. 10.
 * @author KDY
 */
package kr.co.hbilab.dao;

import java.util.List;

import kr.co.hbilab.dto.DeptDTO;

public interface Dao {
    public List<DeptDTO> selectAll();
    public DeptDTO selectOne(int deptno);
    public int insertOne(DeptDTO dto);
    public int updateOne(DeptDTO dto);
    public int deleteOne(int deptno);
}
