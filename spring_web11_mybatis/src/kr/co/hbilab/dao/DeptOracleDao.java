/**
 * 2015. 6. 10.
 * @author KDY
 */
package kr.co.hbilab.dao;

import java.util.List;

import kr.co.hbilab.dto.DeptDTO;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

public class DeptOracleDao implements Dao{
    @Autowired
    SqlSession ss;
    
    @Override
    public List<DeptDTO> selectAll() {
        //System.out.println("Sqlsession : " + ss);
        return ss.selectList("deptSelectAll");
    }

    @Override
    public DeptDTO selectOne(int deptno) {
        return ss.selectOne("deptSelectOne", deptno);
    }

    @Override
    public int updateOne(DeptDTO dto) {
        return ss.update("deptUpdateOne", dto);
    }

    @Override
    public int insertOne(DeptDTO dto) {
        return ss.insert("deptInsertOne", dto);
    }
    
    @Override
    public int deleteOne(int deptno) {
        return ss.delete("deptDeleteOne", deptno);
    }
    
}
