<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
table {
    width:250px;
}
th, td {
    border:1px solid black;
    border-collapse:collapse;
}
th {
    background-color:#dedede;
}
table tr:last-child td {
    border:0px solid black;
    text-align:right;
}
</style>
</head>
<body>
    <form action="delDept" method="post">
        <table>
        	<tr>
        		<th>삭제할 부서번호</th>
        		<td><input type="text" value="${deptno }" name="deptno"/></td>
        	</tr>
        	<tr>
        	   <td colspan="2">
        	       <input type="submit" value="삭제"/>
        	   </td>
        	</tr>
        </table>
    </form>
</body>
</html>