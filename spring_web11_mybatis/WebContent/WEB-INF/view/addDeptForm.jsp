<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
table {
    width:250px;
}
th, td {
    border:1px solid black;
    border-collapse:collapse;
}
th {
    width:65px;
    background-color:#dedede;
}
table tr:last-child td {
    border:0px solid black;
    text-align:right;
}
</style>
</head>
<body>
    <form:form action="addDept" method="post" commandName="dto">
        <table>
        	<tr>
        		<th>Dname</th>
        		<td><form:input path="dname"/></td>
        	</tr>
            <tr>
                <th>Loc</th>
                <td><form:input path="loc"/></td>
            </tr>
        	<tr>
        	   <td colspan="2">
        	       <input type="submit" value="등록"/>
        	   </td>
        	</tr>
        </table>
    </form:form>
</body>
</html>