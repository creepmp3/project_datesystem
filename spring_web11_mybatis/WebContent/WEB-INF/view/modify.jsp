<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
table {
    width:250px;
}
th, td {
    border:1px solid black;
    border-collapse:collapse;
}
th {
    width:65px;
    background-color:#dedede;
}
table tr:last-child td {
    border:0px solid black;
    text-align:right;
}
</style>
</head>
<body>
    <form:form commandName="dto" action="modifyOk" method="post">
        <form:hidden path="deptno" />
	    <table>
	        <tr>
	            <th>DEPTNO</th>
	            <td>${dto.deptno }</td>
	        </tr>
	        <tr>
	            <th>DNAME</th>
	            <td><form:input path="dname" /></td>
	        </tr>
	        <tr>
	            <th>LOC</th>
	            <td><form:input path="loc" /></td>
	        </tr>
	        <tr>
	            <td colspan="2">
                    <a href="deptList"><input type="button" value="목록"/></a>
	                <input type="submit" value="확인"/>
	            </td>
	        </tr>
	    </table>
    </form:form>
</body>
    
</body>
</html>