<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
table {
    width:250px;
}
th, td {
    border:1px solid black;
    border-collapse:collapse;
}
th {
    width:65px;
    background-color:#dedede;
}
table tr:last-child td {
    border:0px solid black;
    text-align:right;
}
</style>
</head>
<body>
    <table>
    	<tr>
    		<th>DEPTNO</th>
    		<td>${dto.deptno }</td>
    	</tr>
    	<tr>
    		<th>DNAME</th>
    		<td>${dto.dname }</td>
    	</tr>
    	<tr>
    		<th>LOC</th>
    		<td>${dto.loc }</td>
    	</tr>
    	<tr>
    	    <td colspan="2">
		        <a href="deptList"><input type="button" value="목록"/></a>
		        <a href="modify?deptno=${dto.deptno }"><input type="button" value="수정" /></a>
		        <a href="delDept?deptno=${dto.deptno }"><input type="button" value="삭제"/></a>
    	    </td>
    	</tr>
    </table>
</body>
</html>