package kr.co.hbilab.app;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

//20150527
public class ConnectionManager {
    static SqlSessionFactory factory;
    
    static{
        try{
            Reader r = Resources.getResourceAsReader("resource/SqlConfig.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            factory = builder.build(r);
            r.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    
    public SqlSessionFactory getFactory(){
        return factory;
    }
}
