package kr.co.hbilab.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 2015. 6. 2.
 * @author KDY
 */

@Controller
@RequestMapping(value="/register")
public class RegisterController {
    
    @RequestMapping(value="/step1")
    public String step1(){
        return "step1";
    }

    @RequestMapping(value="/step2")
    public String step2(){
        return "step2";
    }

    //public String step3(Model model, MemDTO dto){
    //model.addAttribute("dto", dto);

    @RequestMapping(value="/step3")
    public String step3(@ModelAttribute("mem") MemDTO dto){
        return "step3";
    }
    
    
}
