package kr.co.hbilab.web;

/**
 * 2015. 6. 2.
 * @author KDY
 */
public class MemDTO {
    private String id;
    private String pw;
    private String email;
    private int cnt;
    
    
    
    public MemDTO() {
    }

    public MemDTO(String id, String pw, String email, int cnt) {
        super();
        this.id = id;
        this.pw = pw;
        this.email = email;
        this.cnt = cnt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }
    
    
}
