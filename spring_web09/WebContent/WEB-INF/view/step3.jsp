<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript">
$(function(){
    var count = 3;
    setInterval(function(){
        if(count>-1){
            $("#cnt").text(count--);
        }
    }, 1000);

    setTimeout(function(){
        location.href = "../login/login";
    }, 4000);
    
})
</script>
</head>
<body>
    <h3>step3</h3>
    ID : ${mem.id }<br/>
    PW : ${mem.pw }<br/>
    입력하신 이메일은 ${mem.email } 입니다
    <span id="cnt"></span>
</body>
</html>