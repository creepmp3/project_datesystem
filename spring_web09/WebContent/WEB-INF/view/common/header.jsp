<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript">
$(function(){
    $("ul li a").each(function(i, elem){
    	var url = "${url}";
    	var ulUrl = $(elem).attr("href");
    	if(!url.indexOf(ulUrl)){
    		$(elem).css({
    			"font-weight":"bold",
	    		"font-size":"18px",
	    		"text-decoration":"underline"
    		});
    	}
    });
});
</script>
<style type="text/css">
ul li {
    list-style: none;
    float:left;
    margin-right: 5px;
}
ul li a {
    text-decoration: none;
}
</style>
</head>
<body>
    <ul>
        <li><a href="/spring_web09/main">main</a></li>
    	<li><a href="/spring_web09/login">Login</a></li>
    	<li><a href="/spring_web09/member/list">MemberList</a></li>
    </ul>
    <br/>
    <br/>
    <hr/>
    <br/>