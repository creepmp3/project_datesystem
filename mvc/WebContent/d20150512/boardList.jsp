<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">

</script>
<title></title>
</head>
<body>
	<table>
		<tr>
			<th>no</th>
			<th>title</th>
		</tr>
	<c:forEach var="vo" items="${list }">
		<tr>
			<td>${vo.no }</td>
			<td>${vo.title }</td>
		</tr>
	</c:forEach>
	</table>
</body>
</html>