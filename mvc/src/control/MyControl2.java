/**
* Comment  : 
* @version : 1.0
* @author  : Kim Doyeon
* @date    : 2015. 5. 12.
*/
package control;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/myControl2.do")
public class MyControl2 extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }
    
    private void process(HttpServletRequest req, HttpServletResponse resp){
        try {
            req.setCharacterEncoding("UTF-8");
            String cmd = req.getParameter("cmd");
            String msg = "";
            String url = "";
            
            if(cmd==null || cmd.equals("kr")){
                msg = "KOR 안녕하세요";
                url = "/d20150512/kor.jsp";
            }else if(cmd.equals("cn")){
                msg = "CN 니하오";
                url = "/d20150512/cn.jsp";
            }else if(cmd.equals("jp")){
                msg = "JP 곤니찌와";
                url = "/d20150512/jp.jsp";
            }
            req.setAttribute("msg", msg);
            RequestDispatcher dis = req.getRequestDispatcher(url);
            dis.forward(req, resp);
            
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch(ServletException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
