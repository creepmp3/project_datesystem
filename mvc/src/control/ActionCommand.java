/**
* Comment  : 
* @version : 1.0
* @author  : Kim Doyeon
* @date    : 2015. 5. 12.
*/
package control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// 뭔가를 실행하고 결과를 request 의 setAttribute 에 담고 url을 리턴
public interface ActionCommand {
    
    public String execute(HttpServletRequest req, HttpServletResponse resp);
}
