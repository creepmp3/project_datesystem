/**
* Comment  : 
* @version : 1.0
* @author  : Kim Doyeon
* @date    : 2015. 5. 12.
* 
* 파라미터를 구분하여 HelloCommand, IpCommand로 각각 처리
* HelloCommand, IpCommand는 ActionCommand 인터페이스를 상속받기 
*/
package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import command.BoardCommand;
import command.HelloCommand;
import command.IpCommand;

@WebServlet("/myControl3.do")
public class MyControl3 extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPro(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPro(req, resp);
    }
    
    private void doPro(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        
        String type = req.getParameter("type");
        String url = "";
        String msg = "";
        ActionCommand ac = null;
        if(type==null || type.equals("hello")){
            // HelloCommand 객체의 execute 메서드를 실행하면
            // 찾아야할 jsp의 url을 얻어올수 있다
            
            /*HelloCommand hc = new HelloCommand();
            url = hc.execute(req, resp);*/
            ac = new HelloCommand();
        }else if(type.equals("ip")){
            /*IpCommand ic = new IpCommand();
            url = ic.run(req, resp);*/
            ac = new IpCommand();
        }else if(type.equals("board")){
            ac = new BoardCommand();
            // d20150512/boardList.jsp
            // no, title 화면에 출력
        }
        url = ac.execute(req, resp);
        RequestDispatcher dis = req.getRequestDispatcher(url);
        dis.forward(req, resp);
        
    }
}
