/**
* Comment  : 
* @version : 1.0
* @author  : Kim Doyeon
* @date    : 2015. 5. 12.
*/
package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import control.ActionCommand;

public class HelloCommand implements ActionCommand{
    
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp){
        req.setAttribute("msg", "이건 액션커맨드클레스에서 전달해주는 데이터 입니다");
        return "d20150512/hello.jsp";
    }
    
    public HelloCommand(){
    }
}
