/**
* Comment  : 
* @version : 1.0
* @author  : Kim Doyeon
* @date    : 2015. 5. 12.
*/
package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import control.ActionCommand;

public class IpCommand implements ActionCommand{
    public IpCommand(){
    }
    
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp){
        req.setAttribute("msg", req.getRemoteAddr());
        return "d20150512/ip.jsp";
    }
}
