/**
* Comment  : 
* @version : 1.0
* @author  : Kim Doyeon
* @date    : 2015. 5. 12.
*/
package command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import control.ActionCommand;
import dao.BoardDAO;
import dao.BoardVO;

public class BoardCommand implements ActionCommand{
    public BoardCommand(){
    }
    
    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp){
        BoardDAO dao = new BoardDAO();
        ArrayList<BoardVO> list = dao.selectAll(1, 10, "", false);
        req.setAttribute("list", list);
        return "d20150512/boardList.jsp";
    }
}
