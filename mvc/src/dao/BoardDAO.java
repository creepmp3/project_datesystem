package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

// 20150420
public class BoardDAO {
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	StringBuffer sql = new StringBuffer();
	
	public BoardDAO(){
		conn = MakeConnection.getInstance().getConnection();
	}
	
	public ArrayList<BoardVO> selectAll(int startNum, int endNum, String searchKeyword, boolean flag){
		ArrayList<BoardVO> list = new ArrayList<BoardVO>();
		sql.setLength(0);
		sql.append("\n SELECT *                                                                                                              ");
		sql.append("\n   FROM                                                                                                                  ");
		sql.append("\n              (SELECT ROW_NUMBER() OVER(ORDER BY no desc) rnum         ");
		sql.append("\n                             , COUNT(*) OVER()                                              totalcount ");
		sql.append("\n                             , no                                                                                           ");
		sql.append("\n                             , writer                                                                                     ");
		sql.append("\n                             , title                                                                                         ");
		sql.append("\n                             , content                                                                                   ");
		sql.append("\n                             , hits                                                                                        ");
		sql.append("\n                             , wdate                                                                                     ");
		sql.append("\n                             , status                                                                                     ");
		sql.append("\n                  FROM board                                                                                         ");
		if(searchKeyword!=null || !searchKeyword.equals("")){
			sql.append("\n              WHERE title like ?                                                                              ");
			sql.append("\n                       OR content like ?                                                                        ");
		}
		sql.append("\n               )                                                                                                                 ");
		sql.append("\n WHERE rnum BETWEEN "+startNum+" AND "+endNum                              );
		sql.append("\n ORDER BY wdate DESC                                                                                       ");
		try{
			pstmt = conn.prepareStatement(sql.toString());
			if(searchKeyword!=null || !searchKeyword.equals("")){
				pstmt.setString(1, "%"+searchKeyword+"%");
				pstmt.setString(2, "%"+searchKeyword+"%");
			}
			if(flag) System.out.println(sql.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				int totalcount = rs.getInt("totalcount");
				int no = rs.getInt("no");
				String writer = rs.getString("writer");
				String title = rs.getString("title");
				String content = rs.getString("content");
				int hits = rs.getInt("hits");
				String wdate = rs.getString("wdate");
				int status = rs.getInt("status");
				BoardVO vo = new BoardVO(totalcount, no, writer, title, content, hits, wdate, status);
				list.add(vo);
			}
		}catch(SQLException se){
			se.printStackTrace();
		}
		return list;
	}
	
	public ArrayList<BoardVO> searchKeyword(String keyword){
		ArrayList<BoardVO> list = new ArrayList<BoardVO>();
		sql.setLength(0);
		sql.append("\n SELECT * FROM board   ");
		sql.append("\n WHERE title like ?           ");
		sql.append("\n          OR content like ?     ");
		sql.append("\n  ORDER BY wdate DESC ");
		try{
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, "%"+keyword+"%");
			pstmt.setString(2, "%"+keyword+"%");
			rs = pstmt.executeQuery();
			while(rs.next()){
				int no = rs.getInt("no");
				String writer = rs.getString("writer");
				String title = rs.getString("title");
				String content = rs.getString("content");
				int hits = rs.getInt("hits");
				String wdate = rs.getString("wdate");
				int status = rs.getInt("status");
				BoardVO vo = new BoardVO(no, writer, title, content, hits, wdate, status);
				list.add(vo);
			}
		}catch(SQLException se){
			se.printStackTrace();
		}
		return list; 
	}
	
	public int insertOne(BoardVO vo, boolean flag){
		int result = 0;
		sql.setLength(0);
		sql.append("\n INSERT INTO board (no, writer, title, content, hits, wdate, status)");
		sql.append("\n VALUES(board_no_seq.nextval, ?, ?, ?, 0, SYSDATE, 1) ");
		try{
			pstmt = conn.prepareStatement(sql.toString());
			if(flag) System.out.println(sql.toString());
			pstmt.setString(1, vo.getWriter());
			pstmt.setString(2, vo.getTitle());
			pstmt.setString(3, vo.getContent());
			result = pstmt.executeUpdate();
			if(result>0){
				System.out.println("등록성공");
			}else{
				System.out.println("등록실패");
			}
		}catch(SQLException se){
			se.printStackTrace();
		}
		return result;
	}
	
	public BoardVO selectOne(int no, boolean flag){
		BoardVO vo = null;
		
		try{
			
			raiseHits(no);
			
			sql.setLength(0);
			sql.append("\n SELECT * FROM board ");
			sql.append("\n WHERE no = ? ");
			pstmt = conn.prepareStatement(sql.toString());
			if(flag) System.out.println(sql.toString());
			pstmt.setInt(1, no);
			rs = pstmt.executeQuery();
			if(rs.next()){
				String writer = rs.getString("writer");
				String title = rs.getString("title");
				String content = rs.getString("content");
				int hits = rs.getInt("hits");
				String wdate = rs.getString("wdate");
				int status = rs.getInt("status");
				vo = new BoardVO(no, writer, title, content, hits, wdate, status);
			}
		}catch(SQLException se){
			se.printStackTrace();
		}
		return vo;
	}
	
	public void raiseHits(int no){
		sql.setLength(0);
		sql.append("\n UPDATE board SET ");
		sql.append("\n hits = hits+1 ");
		sql.append("\n WHERE no = ? ");
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, no);
			pstmt.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int updateOne(BoardVO vo, boolean flag){
		int result = 0;
		sql.setLength(0);
		sql.append("\n UPDATE board SET ");
		sql.append("\n title = ?, content = ? ");
		sql.append("\n WHERE no = ? ");
		try{
			pstmt = conn.prepareStatement(sql.toString());
			if(flag) System.out.println(sql.toString());
			pstmt.setString(1, vo.getTitle());
			pstmt.setString(2, vo.getTitle());
			pstmt.setInt(3, vo.getNo());
			result = pstmt.executeUpdate();
			if(result>0){
				System.out.println("수정성공");
			}else{
				System.out.println("수정실패");
			}
		}catch(SQLException se){
			se.printStackTrace();
		}
		return result;
	}
	
	public int deleteOne(String no, boolean flag){
		int result = 0;
		sql.setLength(0);
		sql.append("\n DELETE FROM board ");
		sql.append("\n WHERE no = ? ");
		try{
			pstmt = conn.prepareStatement(sql.toString());
			if(flag) System.out.println(sql.toString());
			pstmt.setString(1, no);
			result = pstmt.executeUpdate();
			if(result>0){
				System.out.println("삭제성공");
			}else{
				System.out.println("삭제실패");
			}
		}catch(SQLException se){
			se.printStackTrace();
		}
		return result;
	}
	
}
