package util;

import java.io.File;

public class StringUtil {

	public static String nvl(String str, String repl){
		if(str==null || str.equals("")){
			return repl;
		}
		return str;
	}

	public static String[] nvl(String[] str, String[] repl){
		if(str==null){
			return repl;
		}
		return str;
	}

	public static boolean makeDirectory(String uploadPath){
	    File file = null;
	    String[] arr = uploadPath.split("/");
	    String folder = "/";
	    for(int i=0; i<arr.length; i++){
	        folder += arr[i] + "/";
	        file = new File(folder);
	        if(!file.isDirectory()){
	            file.mkdir();
	        }
	    }
	     file = new File(uploadPath);
	     return (file.isDirectory());
	}
}
