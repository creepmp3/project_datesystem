package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import db.MakeConnection;

/**
 * Member Data Access Object 기능
 *
 * @author 김도연
 * @version 1.0, 2015.4.23
 * @see None
 */
public class MemberDAO {
    int result = 0;
    Connection conn = null;
    PreparedStatement pstmt = null;
    StringBuffer sql = new StringBuffer();
    ResultSet rs = null;
    ResultSetMetaData rsmd = null;
    ArrayList<HashMap<String, String>> list = null;
    HashMap<String, String> map = null;

    public MemberDAO() {
        conn = MakeConnection.getInstance().getConnection();
    }

    public int insertOne(MemberVO vo) {
        sql.setLength(0);
        sql.append("\n INSERT INTO t_member(member_no_n                 ");
        sql.append("\n                    , member_id_v                 ");
        sql.append("\n                    , member_pw_v                 ");
        sql.append("\n                    , member_nick_v               ");
        sql.append("\n                    , member_nm_v                 ");
        sql.append("\n                    , member_gender_v             ");
        sql.append("\n                    , member_bday_v               ");
        sql.append("\n                    , member_email_v              ");
        sql.append("\n                    , member_tel_v                ");
        sql.append("\n                    , member_cell_v               ");
        sql.append("\n                    , member_addr_v               ");
        sql.append("\n                    , member_ip_v                 ");
        sql.append("\n                    , member_use_yn_v             ");
        sql.append("\n                    , member_type_v               ");
        sql.append("\n                    , mem_reg_d                   ");
        sql.append("\n                    )VALUES(                      ");
        sql.append("\n                            member_no_seq.nextval "); // member_no_n
        sql.append("\n                            , ?                   "); // member_id_v
        sql.append("\n                            , ?                   "); // member_pw_v
        sql.append("\n                            , ?                   "); // member_nick_v
        sql.append("\n                            , ?                   "); // member_nm_v
        sql.append("\n                            , ?                   "); // member_gender_v
        sql.append("\n                            , ?                   "); // member_bday_v
        sql.append("\n                            , ?                   "); // member_email_v
        sql.append("\n                            , ?                   "); // member_tel_v
        sql.append("\n                            , ?                   "); // member_cell_v
        sql.append("\n                            , ?                   "); // member_addr_v
        sql.append("\n                            , ?                   "); // member_ip_v
        sql.append("\n                            , 'Y'                 "); // member_use_yn_v
        sql.append("\n                            , ?                   "); // member_type_v
        sql.append("\n                            , SYSDATE             "); // mem_reg_d
        sql.append("\n                           )                      ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            int idx = 1;
            pstmt.setString(idx++, vo.getMember_id_v());
            pstmt.setString(idx++, vo.getMember_pw_v());
            pstmt.setString(idx++, vo.getMember_nick_v());
            pstmt.setString(idx++, vo.getMember_nm_v());
            pstmt.setString(idx++, vo.getMember_gender_v());
            pstmt.setString(idx++, vo.getMember_bday_v());
            pstmt.setString(idx++, vo.getMember_email_v());
            pstmt.setString(idx++, vo.getMember_tel_v());
            pstmt.setString(idx++, vo.getMember_cell_v());
            pstmt.setString(idx++, vo.getMember_addr_v());
            pstmt.setString(idx++, vo.getMember_ip_v());
            pstmt.setString(idx++, vo.getMember_type_v());

            result = pstmt.executeUpdate();

            if (result > 0) {
                System.out.println("등록성공");
            } else {
                System.out.println("등록실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<MemberVO> selectAll() {
        ArrayList<MemberVO> list = new ArrayList<MemberVO>();
        sql.setLength(0);
        sql.append("\n SELECT *                                                                                                                             ");
        sql.append("\n    FROM                                                                                                                                ");
        sql.append("\n        (SELECT ROW_NUMBER() OVER(ORDER BY mem_reg_d DESC) rnum       ");
        sql.append("\n              , COUNT(*) OVER()                            totalcount ");
        sql.append("\n              , member_no_n                                           ");
        sql.append("\n              , member_id_v                                           ");
        sql.append("\n              , member_pw_v                                           ");
        sql.append("\n              , member_nick_v                                         ");
        sql.append("\n              , member_nm_v                                           ");
        sql.append("\n              , member_bday_v                                         ");
        sql.append("\n              , member_email_v                                        ");
        sql.append("\n              , member_tel_v                                          ");
        sql.append("\n              , member_cell_v                                         ");
        sql.append("\n              , member_addr_v                                         ");
        sql.append("\n              , member_ip_v                                           ");
        sql.append("\n              , member_use_yn_v                                       ");
        sql.append("\n              , member_type_v                                         ");
        sql.append("\n              , mem_reg_d                                             ");
        sql.append("\n           FROM t_member                                              ");
        sql.append("\n        )                                                             ");
        // sql.append("\n WHERE rnum BETWEEN 1 AND 10                                        ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            rs = pstmt.executeQuery();

            while(rs.next()) {
                int totalcount = rs.getInt("totalcount");
                int member_no_n = rs.getInt("member_no_n");
                String member_id_v = rs.getString("member_id_v");
                String member_pw_v = rs.getString("member_pw_v");
                String member_nick_v = rs.getString("member_nick_v");
                String member_nm_v = rs.getString("member_nm_v");
                String member_bday_v = rs.getString("member_bday_v");
                String member_email_v = rs.getString("member_email_v");
                String member_tel_v = rs.getString("member_tel_v");
                String member_cell_v = rs.getString("member_cell_v");
                String member_addr_v = rs.getString("member_addr_v");
                String member_ip_v = rs.getString("member_ip_v");
                String member_use_yn_v = rs.getString("member_use_yn_v");
                String member_type_v = rs.getString("member_type_v");
                String mem_reg_d = rs.getString("mem_reg_d");

                MemberVO vo = new MemberVO(totalcount, member_no_n, member_id_v, member_pw_v, member_nick_v,
                        member_nm_v, member_bday_v, member_email_v, member_tel_v, member_cell_v,
                        member_addr_v, member_ip_v, member_use_yn_v, member_type_v, mem_reg_d);
                list.add(vo);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;

    }

    public ArrayList<HashMap<String, String>> getSql2ListMap(StringBuffer sql, boolean flag) {
        int col = 0, j = 0;
        String columnName = "";

        try {
            list = new ArrayList<HashMap<String, String>>();
            pstmt = conn.prepareStatement(sql.toString());

            rs = pstmt.executeQuery();
            if (rs.next()) {
                rsmd = rs.getMetaData();
                col = rsmd.getColumnCount();
                do {
                    map = new HashMap<String, String>();
                    for (j = 1; j <= col; j++) {
                        columnName = rsmd.getColumnName(j).toLowerCase();
                        map.put(columnName, rs.getString(columnName));
                    }
                    list.add(map);
                } while(rs.next());
            }
            if (flag) {
                System.out.println("====================== getSql2ListMap Start ======================");
                System.out.println(sql.toString());
                System.out.println("====================== getSql2ListMap E n d ======================");
            }
        } catch(SQLException se) {
            se.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection(conn, pstmt, rs);
        }
        return list;
    }

    public MemberVO selectOne(int member_no_n) {
        MemberVO vo = null;
        sql.setLength(0);
        sql.append("\n SELECT member_no_n     ");
        sql.append("\n      , member_id_v     ");
        sql.append("\n      , member_pw_v     ");
        sql.append("\n      , member_nick_v   ");
        sql.append("\n      , member_nm_v     ");
        sql.append("\n      , member_bday_v   ");
        sql.append("\n      , member_email_v  ");
        sql.append("\n      , member_tel_v    ");
        sql.append("\n      , member_cell_v   ");
        sql.append("\n      , member_addr_v   ");
        sql.append("\n      , member_ip_v     ");
        sql.append("\n      , member_use_yn_v ");
        sql.append("\n      , member_type_v   ");
        sql.append("\n      , mem_reg_d       ");
        sql.append("\n   FROM t_member        ");
        sql.append("\n  WHERE MEMBER_NO_N = ? ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, member_no_n);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                String member_id_v = rs.getString("member_id_v");
                String member_pw_v = rs.getString("member_pw_v");
                String member_nick_v = rs.getString("member_nick_v");
                String member_nm_v = rs.getString("member_nm_v");
                String member_bday_v = rs.getString("member_bday_v");
                String member_email_v = rs.getString("member_email_v");
                String member_tel_v = rs.getString("member_tel_v");
                String member_cell_v = rs.getString("member_cell_v");
                String member_addr_v = rs.getString("member_addr_v");
                String member_ip_v = rs.getString("member_ip_v");
                String member_use_yn_v = rs.getString("member_use_yn_v");
                String member_type_v = rs.getString("member_type_v");
                String mem_reg_d = rs.getString("mem_reg_d");

                vo = new MemberVO(member_no_n, member_id_v, member_pw_v, member_nick_v, member_nm_v,
                        member_bday_v, member_email_v, member_tel_v, member_cell_v, member_addr_v,
                        member_ip_v, member_use_yn_v, member_type_v, mem_reg_d);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }

        return vo;
    }

    public HashMap<String, String> getSql2Map(StringBuffer sql, boolean flag) {
        int col = 0;
        String columnName = "";

        try {
            map = new HashMap<String, String>();
            pstmt = conn.prepareStatement(sql.toString());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                rsmd = rs.getMetaData();
                col = rsmd.getColumnCount();
                for (int i = 1; i <= col; i++) {
                    columnName = rsmd.getColumnName(i).toLowerCase();
                    map.put(columnName, rs.getString(columnName));
                }
            }
            if (flag) {
                System.out.println("====================== getSql2Map Start ======================");
                System.out.println(sql.toString());
                System.out.println("====================== getSql2Map E n d ======================");
            }
        } catch(SQLException se) {
            se.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection(conn, pstmt, rs);
        }
        return map;
    }

    public int deleteOne(String member_no_n) {
        int result = 0;
        sql.setLength(0);
        sql.append("\n DELETE FROM t_member   ");
        sql.append("\n  WHERE member_no_n = ? ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setString(1, member_no_n);
            result = pstmt.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public MemberVO login(String id, String pw) {
        MemberVO vo = null;
        int count = 0;
        int result = 0;
        sql.setLength(0);
        sql.append("\n SELECT COUNT(*) idcount ");
        sql.append("\n    FROM t_member        ");
        sql.append("\n WHERE member_id_v = ?   ");

        try {

            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                count = rs.getInt("idcount");
                if (count > 0) {
                    result = 1;
                    vo = new MemberVO(result, 0, "", "", "", "");

                    pstmt.close();

                    sql.setLength(0);
                    sql.append("\n SELECT COUNT(*) OVER() pwcount, member_no_n, member_nick_v, member_gender_v, member_type_v, member_cell_v ");
                    sql.append("\n   FROM t_member          ");
                    sql.append("\n WHERE member_id_v = ?    ");
                    sql.append("\n      AND member_pw_v = ? ");

                    pstmt = conn.prepareStatement(sql.toString());
                    pstmt.setString(1, id);
                    pstmt.setString(2, pw);
                    rs = pstmt.executeQuery();
                    if (rs.next()) {
                        count = rs.getInt("pwcount");
                        int member_no_n = rs.getInt("member_no_n");
                        String member_nick_v = rs.getString("member_nick_v");
                        String member_gender_v = rs.getString("member_gender_v");
                        String member_type_v = rs.getString("member_type_v");
                        String member_cell_v = rs.getString("member_cell_v");
                        if (count > 0) {
                            result = 2;
                        }
                        vo = new MemberVO(result, member_no_n, member_nick_v, member_gender_v, member_type_v,
                                member_cell_v);
                    }
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }

        return vo;
    }

    public int idDupl(String str) {
        sql.setLength(0);
        sql.append("\n SELECT COUNT(*) count  ");
        sql.append("\n    FROM t_member       ");
        sql.append("\n  WHERE member_id_v = ? ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setString(1, str);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                result = rs.getInt("count");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void closeConnection(Connection conn, PreparedStatement pstmt, ResultSet rs) {
        try {
            if (rs != null) rs.close();
        } catch(Exception e1) {
        }
        try {
            if (pstmt != null) pstmt.close();
        } catch(Exception e2) {
        }
        try {
            if (conn != null) conn.close();
        } catch(Exception e3) {
        }
    }

}
