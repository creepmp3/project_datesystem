/**
 * FileName : ReservationVO.java
 * Comment  :
 * @version : 1.0
 * @author  : Kim Doyeon
 * @date    : 2015. 4. 30.
 */
package dao;

public class ReservationVO {

    private int totalcount;
    private int reservation_no_n;
    private int stay_no_n;
    private int stay_price_no_n;
    private String user_tel_v;
    private int reservation_range_v;
    private String reservation_date_v;
    private String reservation_time_v;
    private String reservation_hour_v;
    private String reservation_minute_v;
    private String reservation_type_v;
    private String reservation_regd_d;
    private String reservation_use_yn_v;
    private String pay_yn_v;
    private String confirm_yn_v;
    private String stay_title_v;
    private String stay_price_nm_v;
    private int stay_price_n;
    private int total_pay_n;

    public ReservationVO(int stay_no_n, int stay_price_no_n, String user_tel_v, String reservation_date_v,
            String reservation_time_v, String reservation_hour_v, String reservation_minute_v,
            String reservation_type_v, String reservation_regd_d, String reservation_use_yn_v,
            String pay_yn_v, String confirm_yn_v, String stay_title_v, String stay_price_nm_v,
            int stay_price_n) {
        super();
        this.stay_no_n = stay_no_n;
        this.stay_price_no_n = stay_price_no_n;
        this.user_tel_v = user_tel_v;
        this.reservation_date_v = reservation_date_v;
        this.reservation_time_v = reservation_time_v;
        this.reservation_hour_v = reservation_hour_v;
        this.reservation_minute_v = reservation_minute_v;
        this.reservation_type_v = reservation_type_v;
        this.reservation_regd_d = reservation_regd_d;
        this.reservation_use_yn_v = reservation_use_yn_v;
        this.pay_yn_v = pay_yn_v;
        this.confirm_yn_v = confirm_yn_v;
        this.stay_title_v = stay_title_v;
        this.stay_price_nm_v = stay_price_nm_v;
        this.stay_price_n = stay_price_n;
    }

    public ReservationVO(int totalcount, int reservation_no_n, int stay_no_n, int stay_price_no_n,
            String user_tel_v, String reservation_date_v, String reservation_time_v,
            String reservation_type_v, String reservation_regd_d, String pay_yn_v, String confirm_yn_v,
            String stay_title_v, String stay_price_nm_v, int stay_price_n) {
        super();
        this.totalcount = totalcount;
        this.reservation_no_n = reservation_no_n;
        this.stay_no_n = stay_no_n;
        this.stay_price_no_n = stay_price_no_n;
        this.user_tel_v = user_tel_v;
        this.reservation_date_v = reservation_date_v;
        this.reservation_time_v = reservation_time_v;
        this.reservation_type_v = reservation_type_v;
        this.reservation_regd_d = reservation_regd_d;
        this.pay_yn_v = pay_yn_v;
        this.confirm_yn_v = confirm_yn_v;
        this.stay_title_v = stay_title_v;
        this.stay_price_nm_v = stay_price_nm_v;
        this.stay_price_n = stay_price_n;
    }

    public ReservationVO(int stay_no_n, int stay_price_no_n, String user_tel_v, String reservation_date_v,
            String reservation_time_v, String reservation_hour_v, String reservation_minute_v,
            String reservation_type_v, int total_pay_n, String pay_yn_v) {
        super();
        this.stay_no_n = stay_no_n;
        this.stay_price_no_n = stay_price_no_n;
        this.user_tel_v = user_tel_v;
        this.reservation_date_v = reservation_date_v;
        this.reservation_time_v = reservation_time_v;
        this.reservation_hour_v = reservation_hour_v;
        this.reservation_minute_v = reservation_minute_v;
        this.reservation_type_v = reservation_type_v;
        this.total_pay_n = total_pay_n;
        this.pay_yn_v = pay_yn_v;
    }

    public int getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(int totalcount) {
        this.totalcount = totalcount;
    }

    public int getReservation_no_n() {
        return reservation_no_n;
    }

    public void setReservation_no_n(int reservation_no_n) {
        this.reservation_no_n = reservation_no_n;
    }

    public int getStay_no_n() {
        return stay_no_n;
    }

    public void setStay_no_n(int stay_no_n) {
        this.stay_no_n = stay_no_n;
    }

    public int getStay_price_no_n() {
        return stay_price_no_n;
    }

    public void setStay_price_no_n(int stay_price_no_n) {
        this.stay_price_no_n = stay_price_no_n;
    }

    public String getUser_tel_v() {
        return user_tel_v;
    }

    public void setUser_tel_v(String user_tel_v) {
        this.user_tel_v = user_tel_v;
    }

    public int getReservation_range_v() {
        return reservation_range_v;
    }

    public void setReservation_range_v(int reservation_range_v) {
        this.reservation_range_v = reservation_range_v;
    }

    public String getReservation_date_v() {
        return reservation_date_v;
    }

    public void setReservation_date_v(String reservation_date_v) {
        this.reservation_date_v = reservation_date_v;
    }

    public String getReservation_time_v() {
        return reservation_time_v;
    }

    public void setReservation_time_v(String reservation_time_v) {
        this.reservation_time_v = reservation_time_v;
    }

    public String getReservation_hour_v() {
        return reservation_hour_v;
    }

    public void setReservation_hour_v(String reservation_hour_v) {
        this.reservation_hour_v = reservation_hour_v;
    }

    public String getReservation_minute_v() {
        return reservation_minute_v;
    }

    public void setReservation_minute_v(String reservation_minute_v) {
        this.reservation_minute_v = reservation_minute_v;
    }

    public String getReservation_type_v() {
        return reservation_type_v;
    }

    public void setReservation_type_v(String reservation_type_v) {
        this.reservation_type_v = reservation_type_v;
    }

    public String getReservation_regd_d() {
        return reservation_regd_d;
    }

    public void setReservation_regd_d(String reservation_regd_d) {
        this.reservation_regd_d = reservation_regd_d;
    }

    public String getReservation_use_yn_v() {
        return reservation_use_yn_v;
    }

    public void setReservation_use_yn_v(String reservation_use_yn_v) {
        this.reservation_use_yn_v = reservation_use_yn_v;
    }

    public String getStay_title_v() {
        return stay_title_v;
    }

    public void setStay_title_v(String stay_title_v) {
        this.stay_title_v = stay_title_v;
    }

    public String getStay_price_nm_v() {
        return stay_price_nm_v;
    }

    public void setStay_price_nm_v(String stay_price_nm_v) {
        this.stay_price_nm_v = stay_price_nm_v;
    }

    public int getStay_price_n() {
        return stay_price_n;
    }

    public void setStay_price_n(int stay_price_n) {
        this.stay_price_n = stay_price_n;
    }

    public String getPay_yn_v() {
        return pay_yn_v;
    }

    public void setPay_yn_v(String pay_yn_v) {
        this.pay_yn_v = pay_yn_v;
    }

    public String getConfirm_yn_v() {
        return confirm_yn_v;
    }

    public void setConfirm_yn_v(String confirm_yn_v) {
        this.confirm_yn_v = confirm_yn_v;
    }

    public int getTotal_pay_n() {
        return total_pay_n;
    }

    public void setTotal_pay_n(int total_pay_n) {
        this.total_pay_n = total_pay_n;
    }

}
