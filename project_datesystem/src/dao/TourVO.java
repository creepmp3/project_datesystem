package dao;

/*
* final update : 2015-04-24
* writer : 김찬혁
*/

public class TourVO {
	private int totalcount;
	private int tour_no_n;
	private String tour_title_v;
	private String tour_content_v;
	private String tour_tel_v;
	private String tour_open_time_v;
	private String tour_close_time_v;
	private String tour_price_v;
	private String tour_holiday_v;
	private String tour_type_v;
	private String tour_loc_v;
	private String tour_use_yn_v;
	private String tour_reg_d;
	private String tour_imgpath_1_v;
	private String tour_imgpath_2_v;
	private String tour_imgpath_3_v;
	private String tour_imgpath_4_v;
	private String tour_imgpath_5_v;
	private String tour_imgpath_6_v;
	private String tour_imgpath_7_v;
	private String tour_imgpath_8_v;
	
	
	public TourVO() {	}

	
	
	
	public TourVO(int tour_no_n, String tour_title_v, String tour_content_v,
			String tour_tel_v, String tour_open_time_v,
			String tour_close_time_v, String tour_price_v,
			String tour_holiday_v, String tour_type_v, String tour_loc_v,
			String tour_imgpath_1_v, String tour_imgpath_2_v,
			String tour_imgpath_3_v, String tour_imgpath_4_v,
			String tour_imgpath_5_v, String tour_imgpath_6_v,
			String tour_imgpath_7_v, String tour_imgpath_8_v) {
		super();
		this.tour_no_n = tour_no_n;
		this.tour_title_v = tour_title_v;
		this.tour_content_v = tour_content_v;
		this.tour_tel_v = tour_tel_v;
		this.tour_open_time_v = tour_open_time_v;
		this.tour_close_time_v = tour_close_time_v;
		this.tour_price_v = tour_price_v;
		this.tour_holiday_v = tour_holiday_v;
		this.tour_type_v = tour_type_v;
		this.tour_loc_v = tour_loc_v;
		this.tour_imgpath_1_v = tour_imgpath_1_v;
		this.tour_imgpath_2_v = tour_imgpath_2_v;
		this.tour_imgpath_3_v = tour_imgpath_3_v;
		this.tour_imgpath_4_v = tour_imgpath_4_v;
		this.tour_imgpath_5_v = tour_imgpath_5_v;
		this.tour_imgpath_6_v = tour_imgpath_6_v;
		this.tour_imgpath_7_v = tour_imgpath_7_v;
		this.tour_imgpath_8_v = tour_imgpath_8_v;
	}




	public TourVO(int totalcount, int tour_no_n, String tour_title_v,
			String tour_type_v, String tour_loc_v, String tour_reg_d,
			String tour_imgpath_1_v) {
		super();
		this.totalcount = totalcount;
		this.tour_no_n = tour_no_n;
		this.tour_title_v = tour_title_v;
		this.tour_type_v = tour_type_v;
		this.tour_loc_v = tour_loc_v;
		this.tour_reg_d = tour_reg_d;
		this.tour_imgpath_1_v = tour_imgpath_1_v;
	}

	public TourVO(int totalcount, int tour_no_n, String tour_title_v,
			String tour_type_v, String tour_loc_v, String tour_reg_d) {
		super();
		this.totalcount = totalcount;
		this.tour_no_n = tour_no_n;
		this.tour_title_v = tour_title_v;
		this.tour_type_v = tour_type_v;
		this.tour_loc_v = tour_loc_v;
		this.tour_reg_d = tour_reg_d;
	}

	public int getTotalcount() {
		return totalcount;
	}


	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}


	public void setTour_type_v(String tour_type_v) {
		this.tour_type_v = tour_type_v;
	}


	public TourVO(String tour_title_v, String tour_content_v,
			String tour_tel_v, String tour_open_time_v,
			String tour_close_time_v, String tour_price_v,
			String tour_holiday_v, String tour_type_v, String tour_loc_v,
			String tour_use_yn_v, String tour_imgpath_1_v,
			String tour_imgpath_2_v, String tour_imgpath_3_v,
			String tour_imgpath_4_v, String tour_imgpath_5_v,
			String tour_imgpath_6_v, String tour_imgpath_7_v,
			String tour_imgpath_8_v) {
		super();
		this.tour_title_v = tour_title_v;
		this.tour_content_v = tour_content_v;
		this.tour_tel_v = tour_tel_v;
		this.tour_open_time_v = tour_open_time_v;
		this.tour_close_time_v = tour_close_time_v;
		this.tour_price_v = tour_price_v;
		this.tour_holiday_v = tour_holiday_v;
		this.tour_type_v = tour_type_v;
		this.tour_loc_v = tour_loc_v;
		this.tour_use_yn_v = tour_use_yn_v;
		this.tour_imgpath_1_v = tour_imgpath_1_v;
		this.tour_imgpath_2_v = tour_imgpath_2_v;
		this.tour_imgpath_3_v = tour_imgpath_3_v;
		this.tour_imgpath_4_v = tour_imgpath_4_v;
		this.tour_imgpath_5_v = tour_imgpath_5_v;
		this.tour_imgpath_6_v = tour_imgpath_6_v;
		this.tour_imgpath_7_v = tour_imgpath_7_v;
		this.tour_imgpath_8_v = tour_imgpath_8_v;
	}

	public TourVO(String tour_title_v, String tour_content_v,
			String tour_tel_v, String tour_open_time_v,
			String tour_close_time_v, String tour_price_v,
			String tour_holiday_v, String tour_type_v, String tour_loc_v,
			String tour_use_yn_v, String tour_reg_d, String tour_imgpath_1_v,
			String tour_imgpath_2_v, String tour_imgpath_3_v,
			String tour_imgpath_4_v, String tour_imgpath_5_v,
			String tour_imgpath_6_v, String tour_imgpath_7_v,
			String tour_imgpath_8_v){
		
		this.tour_title_v = tour_title_v;
		this.tour_content_v = tour_content_v;
		this.tour_tel_v = tour_tel_v;
		this.tour_open_time_v = tour_open_time_v;
		this.tour_close_time_v = tour_close_time_v;
		this.tour_price_v = tour_price_v;
		this.tour_holiday_v = tour_holiday_v;
		this.tour_type_v = tour_type_v;
		this.tour_loc_v = tour_loc_v;
		this.tour_use_yn_v = tour_use_yn_v;
		this.tour_reg_d = tour_reg_d;
		this.tour_imgpath_1_v = tour_imgpath_1_v;
		this.tour_imgpath_2_v = tour_imgpath_2_v;
		this.tour_imgpath_3_v = tour_imgpath_3_v;
		this.tour_imgpath_4_v = tour_imgpath_4_v;
		this.tour_imgpath_5_v = tour_imgpath_5_v;
		this.tour_imgpath_6_v = tour_imgpath_6_v;
		this.tour_imgpath_7_v = tour_imgpath_7_v;
		this.tour_imgpath_8_v = tour_imgpath_8_v;
	}

	public TourVO(int tour_no_n, String tour_title_v, String tour_content_v,
			String tour_tel_v, String tour_open_time_v,
			String tour_close_time_v, String tour_price_v,
			String tour_holiday_v, String tour_type_v, String tour_loc_v,
			String tour_use_yn_v, String tour_reg_d, String tour_imgpath_1_v,
			String tour_imgpath_2_v, String tour_imgpath_3_v,
			String tour_imgpath_4_v, String tour_imgpath_5_v,
			String tour_imgpath_6_v, String tour_imgpath_7_v,
			String tour_imgpath_8_v) {
		
		this.tour_no_n = tour_no_n;
		this.tour_title_v = tour_title_v;
		this.tour_content_v = tour_content_v;
		this.tour_tel_v = tour_tel_v;
		this.tour_open_time_v = tour_open_time_v;
		this.tour_close_time_v = tour_close_time_v;
		this.tour_price_v = tour_price_v;
		this.tour_holiday_v = tour_holiday_v;
		this.tour_type_v = tour_type_v;
		this.tour_loc_v = tour_loc_v;
		this.tour_use_yn_v = tour_use_yn_v;
		this.tour_reg_d = tour_reg_d;
		this.tour_imgpath_1_v = tour_imgpath_1_v;
		this.tour_imgpath_2_v = tour_imgpath_2_v;
		this.tour_imgpath_3_v = tour_imgpath_3_v;
		this.tour_imgpath_4_v = tour_imgpath_4_v;
		this.tour_imgpath_5_v = tour_imgpath_5_v;
		this.tour_imgpath_6_v = tour_imgpath_6_v;
		this.tour_imgpath_7_v = tour_imgpath_7_v;
		this.tour_imgpath_8_v = tour_imgpath_8_v;
	}


	public int getTour_no_n() {
		return tour_no_n;
	}


	public void setTour_no_n(int tour_no_n) {
		this.tour_no_n = tour_no_n;
	}


	public String getTour_title_v() {
		return tour_title_v;
	}


	public void setTour_title_v(String tour_title_v) {
		this.tour_title_v = tour_title_v;
	}


	public String getTour_content_v() {
		return tour_content_v;
	}


	public void setTour_content_v(String tour_content_v) {
		this.tour_content_v = tour_content_v;
	}


	public String getTour_tel_v() {
		return tour_tel_v;
	}


	public void setTour_tel_v(String tour_tel_v) {
		this.tour_tel_v = tour_tel_v;
	}


	public String getTour_open_time_v() {
		return tour_open_time_v;
	}


	public void setTour_open_time_v(String tour_open_time_v) {
		this.tour_open_time_v = tour_open_time_v;
	}


	public String getTour_close_time_v() {
		return tour_close_time_v;
	}


	public void setTour_close_time_v(String tour_close_time_v) {
		this.tour_close_time_v = tour_close_time_v;
	}


	public String getTour_price_v() {
		return tour_price_v;
	}


	public void setTour_price_v(String tour_price_v) {
		this.tour_price_v = tour_price_v;
	}


	public String getTour_holiday_v() {
		return tour_holiday_v;
	}


	public void setTour_holiday_v(String tour_holiday_v) {
		this.tour_holiday_v = tour_holiday_v;
	}


	public String getTour_type_v() {
		return tour_type_v;
	}


	public void settour_type_v(String tour_type_v) {
		this.tour_type_v = tour_type_v;
	}


	public String getTour_loc_v() {
		return tour_loc_v;
	}


	public void setTour_loc_v(String tour_loc_v) {
		this.tour_loc_v = tour_loc_v;
	}


	public String getTour_use_yn_v() {
		return tour_use_yn_v;
	}


	public void setTour_use_yn_v(String tour_use_yn_v) {
		this.tour_use_yn_v = tour_use_yn_v;
	}


	public String getTour_reg_d() {
		return tour_reg_d;
	}


	public void setTour_reg_d(String tour_reg_d) {
		this.tour_reg_d = tour_reg_d;
	}


	public String getTour_imgpath_1_v() {
		return tour_imgpath_1_v;
	}


	public void setTour_imgpath_1_v(String tour_imgpath_1_v) {
		this.tour_imgpath_1_v = tour_imgpath_1_v;
	}


	public String getTour_imgpath_2_v() {
		return tour_imgpath_2_v;
	}


	public void setTour_imgpath_2_v(String tour_imgpath_2_v) {
		this.tour_imgpath_2_v = tour_imgpath_2_v;
	}


	public String getTour_imgpath_3_v() {
		return tour_imgpath_3_v;
	}


	public void setTour_imgpath_3_v(String tour_imgpath_3_v) {
		this.tour_imgpath_3_v = tour_imgpath_3_v;
	}


	public String getTour_imgpath_4_v() {
		return tour_imgpath_4_v;
	}


	public void setTour_imgpath_4_v(String tour_imgpath_4_v) {
		this.tour_imgpath_4_v = tour_imgpath_4_v;
	}


	public String getTour_imgpath_5_v() {
		return tour_imgpath_5_v;
	}


	public void setTour_imgpath_5_v(String tour_imgpath_5_v) {
		this.tour_imgpath_5_v = tour_imgpath_5_v;
	}


	public String getTour_imgpath_6_v() {
		return tour_imgpath_6_v;
	}


	public void setTour_imgpath_6_v(String tour_imgpath_6_v) {
		this.tour_imgpath_6_v = tour_imgpath_6_v;
	}


	public String getTour_imgpath_7_v() {
		return tour_imgpath_7_v;
	}


	public void setTour_imgpath_7_v(String tour_imgpath_7_v) {
		this.tour_imgpath_7_v = tour_imgpath_7_v;
	}


	public String getTour_imgpath_8_v() {
		return tour_imgpath_8_v;
	}


	public void setTour_imgpath_8_v(String tour_imgpath_8_v) {
		this.tour_imgpath_8_v = tour_imgpath_8_v;
	}	
	
}
