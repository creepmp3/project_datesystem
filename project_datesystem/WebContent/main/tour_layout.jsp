<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="../css/main/main_layout.css" />
<link rel="stylesheet" href="../css/main/top_menu.css" />
<link rel="stylesheet" href="../css/main/top_menu.css" />
<link rel="stylesheet" href="../css/main/tour_style.css" />
</head>
<body>
	<div id="content">
		<jsp:include page="menubar.jsp"></jsp:include>
		<div id="checklist">
			<div id="checktext">
				<br />지역<br />세부사항
			</div>
			<div id="check_box">
				<ul>
					<!-- 컨텐츠 추가 및 삭제는 추후에 진행함 지역구를 어떻게 나눠야 할지 모르겠음 -->
					<li><input type="checkbox" name="" id="chb">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
				</ul>
			</div>
			<div id="checktext">
				<br />테마<br />세부사항
			</div>
			<div id="check_box">
				<ul>
					<!--테마 나누는게 힘듬... 한식 일식 양식 중식 인도 이탈리안 카페 이런식은 어떤지?? -->
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
				</ul>
			</div>
		</div>

		<div id="list_search">
			<input type="button" name="" id="search_btn" value="search" /> <input
				type="text" name="" id="search" />
		</div>

		<div id="search_box">
			<div id="line">
				<h2>맛있게 먹자!!</h2>
			</div>
			<ul>
				<li>전체</li>
				<li>추천</li>
				<li>인기</li>
			</ul>
		</div>
		<jsp:include page="cook.jsp"></jsp:include>

	</div>
</body>
</html>