<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/main/sub_contents_style.css"/>
<title>Insert title here</title>
</head>
<body>
	<div id="sub_content">
		<div id="content_image">
			<img src="../images/slide/sub/stay_sub_1.jpg">
		</div>
		<div id="com">
			<div id="weather">
				<div class="row">
					<div class="col-sm-8">
						<div id="daily_list"></div>
					</div>
				</div>
			</div>
			<div id="board">커뮤니티 게시판</div>
		</div>
	</div>
</body>
</html>