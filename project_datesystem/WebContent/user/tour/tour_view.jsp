<%@page import="dao.TourVO"%>
<%@page import="dao.TourDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<html>
<head>
<title></title>
<link rel="stylesheet" href="/project_datesystem/css/table.css" />
<link rel="stylesheet" href="../../css/main/main_layout.css" />
<link rel="stylesheet" href="../../css/main/top_menu.css" />
<link rel="stylesheet" href="../../css/main/tour_style.css" />
<script type="text/javascript" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("img[name='subImg']").each(function(i,e){
		$(e).on('mouseover',function(){
			$("#titleImg").attr("src",($(e).attr("src")));
		});
	});
});
</script>
<style type="text/css">
#subImg{
	width:80px;
	height:30px;
	float:left;
}
#titleImg{
	width:500px;
	height:320px;
	padding: 0px auto;
}
table{
	width:930px;
}
th{
	width:100px;
	text-align: right;
}
td{
	text-align: left;
}
th,td{
	border: 1px solid black;
	padding: 1px;
}
#imgTable{
	width:600px;
	border: 0px;
	margin-left:160px;
}
.imgTd{
	margin:0px;
	padding:0px auto;
}
#titleImgTd{
	width:800px;
	padding:0px 50px;
}
.topth{
	text-align: center;
	border: 0px;
}
.toptr{
	text-align: center;
	border: 0px;
}
.imgTd{
	border: 0px;
}
#titleImgTd{
	border:0px;
}
</style>
</head>
<body>
	<div id="content">
	<div id = "main">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		<%
			String no = request.getParameter("tour_no_n");
			int tno = 0;
			TourVO vo = null;
			if(no!=null){
				tno = Integer.parseInt(no);
				TourDAO dao = new TourDAO();
				vo = dao.selectOne(tno);
			}
		%>
		<div id="divImg">
		<table>
			<%
				String type = "";
			    if(vo.getTour_type_v().equals("place")){
			        type="명소";
			%>
			<tr class="toptr">
				<th class="topth" colspan="2"><%=type %></th>
			</tr>
			<% 
			    }else if(vo.getTour_type_v().equals("working")){
			        type="산책로";
			%>
			<tr class="toptr">
				<th class="topth" colspan="2"><%=type %></th>
			</tr>
			<%
			    }
			%>
			
			<tr class="toptr">
				<th class="topth" colspan="2"><%=vo.getTour_title_v() %></th>
			</tr>
		</table>
		
		<table id="imgTable">
			<tr class="imgbox">
				<td id="titleImgTd"colspan="8"><img id="titleImg" name="subImg" src="<%=vo.getTour_imgpath_1_v() %>" alt="" /></td>
			</tr>
			<tr>
			<%
				String[] str = new String[8];
				str[0] = vo.getTour_imgpath_1_v();
				str[1] = vo.getTour_imgpath_2_v();
				str[2] = vo.getTour_imgpath_3_v();
				str[3] = vo.getTour_imgpath_4_v();
				str[4] = vo.getTour_imgpath_5_v();
				str[5] = vo.getTour_imgpath_6_v();
				str[6] = vo.getTour_imgpath_7_v();
				str[7] = vo.getTour_imgpath_8_v();
				for(int i=0; i<str.length; i++){
					if(str[i].endsWith("null")){
					}else{
			%>
						
				<td class="imgTd"><img id="subImg" name="subImg" src="<%=str[i] %>" alt="subimg" /></td>
			
			<%		
					}
				}
			%>
			</tr>
		</table>
		
		<table>
			<tr>
				<th>위 치 : </th>
				<td><%=vo.getTour_loc_v() %></td>
			</tr>
			<tr>
				<th>설 명 : </th>
				<td><%=vo.getTour_content_v() %></td>
			</tr>
			<tr>
				<th>오픈시간 : </th>
				<td><%=vo.getTour_open_time_v() %></td>
			</tr>
			<tr>
				<th>닫는시간 : </th>
				<td><%=vo.getTour_close_time_v() %></td>
			</tr>
			<tr>
				<th>휴 일 : </th>
				<td><%=vo.getTour_holiday_v() %></td>
			</tr>
			<tr>
				<th>전화번호 : </th>
				<td><%=vo.getTour_tel_v() %></td>
			</tr>
		</table>
		</div>
	</div>
	</div>
<%@ include file="/common/sub_footer.jsp" %>
</body>
</html>