<%@page import="com.sun.xml.internal.fastinfoset.Encoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.FoodDAO, dao.FoodVO, util.StringUtil" %>
<%@ page import="db.MakeConnection, java.sql.*"%>
<%@ include file="/common/sub_header.jsp" %>
<%
	request.setCharacterEncoding("UTF-8");

	String member_id_v = StringUtil.nvl((String)session.getAttribute("member_id_v"), "");
	int food_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("food_no_n"), "7"));
	
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!--  jQuery 기본 js파일 -->
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=b997e5bb20d6753e8f9185256ba673b2&libraries=services"></script>
<script type="text/javascript" src="/project_datesystem/js/daum_map.js"></script>

<script type="text/javascript" src="http://connect.facebook.net/ko_KR/all.js"></script>

<link rel="stylesheet" type="text/css" href="../../css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/menu_content.css" />
<link rel="stylesheet" href="../css/content_css/tour_content_style.css" />
<link rel="stylesheet" href="../../css/main/tour_style.css" />
<script type="text/javascript">
$(document).ready(function(){
	
	

	/* ****************************** facebook *********************************** */
	
	// This is called with the results from from FB.getLoginStatus().
	  function statusChangeCallback(response) {
	    console.log('statusChangeCallback');
	    console.log(response);
	    // The response object is returned with a status field that lets the
	    // app know the current login status of the person.
	    // Full docs on the response object can be found in the documentation
	    // for FB.getLoginStatus().
	    if (response.status === 'connected') {
	      // Logged into your app and Facebook.
	      testAPI();
	    } else if (response.status === 'not_authorized') {
	      // The person is logged into Facebook, but not your app.
	      document.getElementById('status').innerHTML = 'Please log ' +
	        'into this app.';
	    } else {
	      // The person is not logged into Facebook, so we're not sure if
	      // they are logged into this app or not.
	      document.getElementById('status').innerHTML = 'Please log ' +
	        'into Facebook.';
	    }
	  }

	  // This function is called when someone finishes with the Login
	  // Button.  See the onlogin handler attached to it in the sample
	  // code below.
	  function checkLoginState() {
	    FB.getLoginStatus(function(response) {
	      statusChangeCallback(response);
	    });
	  }

	  window.fbAsyncInit = function() {
		  FB.init({
		    appId      : '{1611932895685627}',
		    cookie     : true,  // enable cookies to allow the server to access 
		                        // the session
		    xfbml      : true,  // parse social plugins on this page
		    version    : 'v2.2' // use version 2.2
		  });
	
		  // Now that we've initialized the JavaScript SDK, we call 
		  // FB.getLoginStatus().  This function gets the state of the
		  // person visiting this page and can return one of three states to
		  // the callback you provide.  They can be:
		  //
		  // 1. Logged into your app ('connected')
		  // 2. Logged into Facebook, but not your app ('not_authorized')
		  // 3. Not logged into Facebook and can't tell if they are logged into
		  //    your app or not.
		  //
		  // These three cases are handled in the callback function.
	
		  FB.getLoginStatus(function(response) {
		      
			statusChangeCallback(response);
		      
		  });
		  
	  };

	  // Load the SDK asynchronously
	  (function(d, s, id) {
	    var js, fjs = d.getElementsByTagName(s)[0];
	    if (d.getElementById(id)) return;
	    js = d.createElement(s); js.id = id;
	    js.src = "//connect.facebook.net/ko_KR/sdk.js#xfbml=1&version=v2.3&appId=1611932895685627";
	    fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));
	  
	  // Here we run a very simple test of the Graph API after login is
	  // successful.  See statusChangeCallback() for when this call is made.
	  function testAPI() {
	    console.log('Welcome!  Fetching your information.... ');
	    FB.api('/me', function(response) {
	      console.log('Successful login for: ' + response.name);
	      document.getElementById('status').innerHTML =
	        'Thanks for logging in, ' + response.name + '!';
	    });
	  }
	
	/* ****************************** facebook *********************************** */
	
	
	$("#addr").hide();
	$("#payView").hide();
	$("#mapView").hide();
	$("#infoView").show();
	
	$("#infoBtn").on("click", function(){
		$("#payView").hide(500);
		$("#mapView").hide(500);
		$("#infoView").show(500);
		$("#tab li").each(function(i, elem){
			$(elem).removeClass("selected_tab");
		});
		$(this).parent().addClass("selected_tab");
		return false;
	});
	
	$("#payBtn").on("click", function(){
		$(".shareBox").hide();
		$("#infoView").hide(500);
		$("#payView").show(500);
		$("#mapView").hide(500);
		$("#tab li").each(function(i, elem){
			$(elem).removeClass("selected_tab");
		});
		$(this).parent().addClass("selected_tab");
		return false;
	});
	
	$("#mapBtn").on("click", function(){
		$(".shareBox").hide();
		$("#infoView").hide(500);
		$("#payView").hide(500);
		$("#tab li").each(function(i, elem){
			$(elem).removeClass("selected_tab");
		});
		$(this).parent().addClass("selected_tab");
		var title = $("#title").text();
		var addr = $("#addr").text();
		console.log(addr, title);
		$("#mapView").show(500);
		
		map(addr, title);
		
		return false;
	});
	
	
	$(".choice2").on("click", function(){
		$(".shareBox").slideToggle(500);
		return false;
	});
	
	$("img[name='foodImg']").each(function(i, elem){
		$(elem).on("mouseover", function(){
			var src = $(elem).attr("src");
			$("#titleImg").attr("src", src);
		});
	});

	
	var food_no_n = "<%=food_no_n%>";
	
	$(".choice1").click(function(){
		var recommend = $(".recommend").text();
		
		if("<%=member_id_v%>" == ""){
			alert("로그인이 필요합니다");
			$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
			return false;
		}else{
			$.getJSON(
					 "recommend_update.jsp"
					, {"food_no_n":<%=food_no_n%>
					, "recommend":recommend}					 
					, function(data){
						 if(data.message == "duplicate"){
							 alert("추천은 1회만 가능합니다.");
						 }else{
							 alert("추천하였습니다");
						 	$(".recommend").text(data.recommend);
						 }
					  }
			);
		}
		return false
	});
});

</script>

<style type="text/css">
#list_search {
	width: 930px;
	height: 100px;
	margin-top: 25px;
	border-radius: 15px;
	box-shadow: 2px 2px 3px #FF70B2;
	position: relative;
	overflow: hidden;
	float: left;
	position: relative
}
#tour_content_top {
	width: 930px;
	height: 40px;
	border-bottom: 3px outset #FF70B2;
	padding-top: 10px;
	padding-left: 10px; color : white;
	font-size: 20px;
	font-weight: bold; background : linear-gradient( to top, #FF70B2,
	#FFA5BE);
	position: relative;
	float: left;
	background: linear-gradient(to top, #FF70B2, #FFA5BE);
	color: white
}
#search_box{
	margin-bottom: -10px;
}
div.imgBox{
 	position:relative;
	left:0px;
	top:45px;
	width:480px;
	height:320px;
	border:0px solid black;

}
dl.imgDl{
	list-style: none !important;
}

dl.imgDl dt img{
	width:455px;
	height:200px;
}
dl.imgDl dd{
	display:inline !important;
	float:left !important;
	margin-right:5px;
}

dl.imgDl dd img{
	width:110px;
	height:65px;
}

div.infoBox {
	width:400px;
	position:relative;
	left:500px;
	top: -280px;
}
#infoView, #payView, #mapView{
	height:400px;
}
#mapView{
 	position:relative;
	left:180px;
	top:0px;
}
.footer {
	clear:both;
	margin-top:100px !important;
	margin-bottom:100px !important;
}

#tab {
	margin-top:10px !important;
}
#tab li{
	font-size: 20px;
	background:#eee;
	margin-left: 0px !important;
	width: 70px;
	text-align: center;
	border:1px solid #ccc !important;
	border-top-right-radius:5px;
	border-top-left-radius:5px;
}

#tab li:HOVER, #tab li:FOCUS, #tab li.selected_tab {
	border-bottom:1px solid #fff !important;
	background:#fff; color:#f60;
}

.selected_tab {
}
#tab li a {
	display:block;
}
.contentView{
	margin-top:5px;
	clear:both;
	border:1px solid #ccc;
	padding:20px;
}

.choiceBox {
	margin:0px;
	padding:0px;
}
.choice1 {
 	position:relative;
	width:59px;
	height:57px;
	top:25px;
	left:121px;
	display: inline-block;
	background:url('/project_datesystem/images/choice_1.gif') left top no-repeat;
}
.choice2 {
 	position:relative;
	width:59px;
	height:57px;
	top:65px;
	left:120px;
	display: inline-block;
	background:url('/project_datesystem/images/choice_2.gif') left top no-repeat;
}

.recommend {
	width:59px;
	display: inline-block;
 	position:relative;
 	text-align:center;
	top:5px;
	left:5px;
	color:#FF5E80;
	font-weight:bold;
}
.shareBox {
 	position:relative;
	top:30px;
	left:120px;
	width:200px;
	height:100px;
	border:0px solid #FF5E80;
	display:none;
	text-align:center;
}
.resBtn {
	width: 63px;
	height: 30px;
	padding:5px 15px 5px 15px;
	text-shadow : 3px 3px 3px #FF70B2;
	border: 1px solid #FF70B2;
	color : white;
	font-size : 15px;
	font-weight : bold;
	background: linear-gradient(to top, #FF70B2, #FFA5BE);
	border-radius: 10px;
}

</style>
</head>
<body>
<%

	FoodDAO dao = new FoodDAO();	
	FoodVO vo = dao.selectOne(food_no_n);
	
		
	String type = "";
	if(vo.getFood_type_v().equals("K")){
	    type="한식";
	}else if(vo.getFood_type_v().equals("C")){
	    type="중식";
	}else if(vo.getFood_type_v().equals("J")){
	    type="일식";
	}
%>
<form id="moveForm">
	<input type="hidden" name="retUrl" value="<%=PAGENAME%>?food_no_n=<%=food_no_n%>"/>
</form>
	<div id="main">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		
		<div id="list_search">
			<div id="tour_content_top"><%=vo.getFood_title_v()%></div>
			<div id="tour_content_down">
				
			</div>
		</div>
		<div id="search_box">
			<ul id="tab">
				<li class="selected_tab"><a href="#" id="infoBtn">정보</a></li>
				<li><a href="#" id="payBtn">요금</a></li>
				<li><a href="#" id="mapBtn">약도</a></li>
			</ul>
		</div>
		<div class="contentView">
			<div id="infoView">
				<div class="imgBox">
					<dl class="imgDl">
						<dt><img id="titleImg" name="foodImg" src="<%=vo.getFood_imgpath_1_v() %>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></dt>
						<dd><img name="foodImg" src="<%=vo.getFood_imgpath_2_v() %>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></dd>
						<dd><img name="foodImg" src="<%=vo.getFood_imgpath_3_v() %>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></dd>
						<dd><img name="foodImg" src="<%=vo.getFood_imgpath_4_v() %>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></dd>
						<dd><img name="foodImg" src="<%=vo.getFood_imgpath_5_v() %>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></dd>
					</dl>
				</div>
				<div class="infoBox">
					<p><%=vo.getFood_info_title_v() %></p>
					<p>지역 : <%=vo.getFood_loc_v() %></p>
					<p>종류 : <%=type %></p>
					<p>영업시간 : <%=vo.getFood_open_time_v() %> ~ <%=vo.getFood_close_time_v() %></p>
					<p>휴일 : <%=vo.getFood_holiday_v() %></p>
					<p>전화 : <%=vo.getFood_tel_v() %> </p>
					<span id="readCnt">조회수 <%=vo.getFood_read_n() %></span>
					<div class="choiceBox">
						<a href="#" class="choice1"><span class="recommend"><%=vo.getFood_recommend_n() %></span></a>
						<a href="#" class="choice2"><span></span></a>
						
					</div>
					<div class="shareBox">
						<div class="fb-share-button" data-href="<%=PAGENAME %>?food_no_n=<%=food_no_n %>" data-layout="button"></div>
						
					</div>
				</div>
			</div>
			<div id="payView">
				<%-- <div class="list">
					<table>
						<thead>
							<tr>
								<th rowspan="2">구분</th>
								<th colspan="2">대실</th>
								<th colspan="2">숙박</th>
							</tr>
							<tr>
								<td>월~금</td>
								<td>토~일</td>
								<td>월~금</td>
								<td>토~일</td>
							</tr>
						</thead>
						<tbody>
							<%
							for(PriceVO pvo : paylist){
							%>
							<tr>
								<th><%=pvo.getStay_price_nm_v() %></th>
								<td><%=pvo.getStay_d_w_v() %></td>
								<td><%=pvo.getStay_d_h_v() %></td>
								<td><%=pvo.getStay_s_w_v() %></td>
								<td><%=pvo.getStay_s_h_v() %></td>
							</tr>
							<%
							}
							%>
						</tbody>
					</table>
				</div> --%>
			</div>
			<div id="mapView">
				<span id="addr"><%=vo.getFood_loc_v() %></span>
				<a target="_blank" href="http://map.daum.net/?map_type=TYPE_MAP&q=<%= vo.getFood_loc_v() %>&urlY=1130425&urlLevel=3">
					<div id="map" style="text-align:cetner;width:500px;height:350px;"></div>
				</a>
			</div>
			<div class="fb-comments" data-href="http://www.baemin.com/" data-numposts="5" data-colorscheme="light">Facebook Comments Not Loading</div>
			
			<div class="footer"></div>
			<div style="clear:both;float:right">
				<a href="./reservation/reservation_calendar.jsp?food_no_n=<%=food_no_n %>" class="resBtn">예약</a>
			</div>
			<div class="footer"></div>
		</div>
	</div>
</body>
</html>