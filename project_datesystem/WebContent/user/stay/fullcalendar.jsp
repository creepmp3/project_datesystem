<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">

</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>

<link rel='stylesheet' href='../../js/fullcalendar-2.3.1/lib/cupertino/jquery-ui.min.css' />
<link href='../../js/fullcalendar-2.3.1/fullcalendar.css' rel='stylesheet' />
<link href='../../js/fullcalendar-2.3.1/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='../../js/fullcalendar-2.3.1/lib/moment.min.js'></script><!-- 
<script src='../../js/fullcalendar-2.3.1/lib/jquery.min.js'></script> -->
<script src='../../js/fullcalendar-2.3.1/fullcalendar.min.js'></script>
<script src='../../js/fullcalendar-2.3.1/lang-all.js'></script>

<script type="text/javascript">
$(document).ready(function() {
	var currentLangCode = 'ko';

	function renderCalendar() {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '2015-04-29',
			lang: currentLangCode,
			buttonIcons: false, // show the prev/next text
			weekNumbers: true,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				{
					title: 'All Day Event',
					start: '2015-04-01'
				},
				{
					title: 'Long Event',
					start: '2015-04-07',
					end: '2015-04-10'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2015-04-09T16:00:00'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2015-04-16T16:00:00'
				},
				{
					title: 'Conference',
					start: '2015-04-11',
					end: '2015-04-13'
				},
				{
					title: 'Meeting',
					start: '2015-04-12T10:30:00',
					end: '2015-04-12T12:30:00'
				},
				{
					title: 'Lunch',
					start: '2015-04-12T12:00:00'
				},
				{
					title: 'Meeting',
					start: '2015-04-12T14:30:00'
				},
				{
					title: 'Happy Hour',
					start: '2015-04-12T17:30:00'
				},
				{
					title: 'Dinner',
					start: '2015-04-12T20:00:00'
				},
				{
					title: 'Birthday Party',
					start: '2015-04-13T07:00:00'
				},
				{
					title: 'Click for Google',
					url: 'http://google.com/',
					start: '2015-04-28'
				}
			]
		});
	}

	renderCalendar();
});
</script>
<title></title>
</head>
<body>
	<div id='calendar'></div>
</body>
</html>