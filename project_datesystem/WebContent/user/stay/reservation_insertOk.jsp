<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.ReservationDAO, dao.ReservationVO, util.StringUtil, java.util.Enumeration" %>
<%
	/* 	Enumeration enums = request.getParameterNames();
	out.println(enums.hasMoreElements());
	while (enums.hasMoreElements()){      
		String name = (String)enums.nextElement();      
		String value = request.getParameter(name);
		
		out.println(name+" : <b>" + value + "</b> , <br/>");
	} */
	
	String user_tel_v = StringUtil.nvl(request.getParameter("user_tel_v"), "");
	String reservation_date_v = StringUtil.nvl(request.getParameter("reservation_date_v"), "");
	String reservation_time_v = StringUtil.nvl(request.getParameter("reservation_time_v"), "");
	String reservation_day_v = StringUtil.nvl(request.getParameter("reservation_day_v"), "");
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	int stay_price_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_price_no_n"), "0"));
	int reservation_range_v = Integer.parseInt(StringUtil.nvl(request.getParameter("reservation_range_v"), "0"));
	String stay_price_type_v = StringUtil.nvl(request.getParameter("stay_price_type_v"), "");
	String stay_price_type2_v = StringUtil.nvl(request.getParameter("stay_price_type2_v"), "");
	String pay_yn_v = StringUtil.nvl(request.getParameter("pay_yn_v"), "");
	String reservation_type_v = "";
	
	if(stay_price_type_v.equals("숙박")){
	    if(stay_price_type2_v.equals("평일")){
	    	reservation_type_v = "sw";
	    }else if(stay_price_type2_v.equals("주말")){
	        reservation_type_v = "sh";
	    }
	}if(stay_price_type_v.equals("대실")){
	    if(stay_price_type2_v.equals("평일")){
	    	reservation_type_v = "dw";
	    }else if(stay_price_type2_v.equals("주말")){
	        reservation_type_v = "dh";
	    }
	}
	
	String[] timeSplit = reservation_time_v.split(":");
	
	String yearMonth = reservation_date_v.substring(0, reservation_date_v.lastIndexOf("-")+1);
	int day = Integer.parseInt(reservation_date_v.substring(reservation_date_v.lastIndexOf("-")+1, reservation_date_v.length()));
	
	String reservationDate = ""; 
	
	ReservationDAO dao = new ReservationDAO();
	ReservationVO vo = null;
	int result = 0;
	
	for(int i=0; i<timeSplit.length; i++){
		System.out.println("timeSplit : " +timeSplit[i]);
	}
	
	for(int i=0; i<reservation_range_v; i++){
	    int d = day+i;
	    reservationDate = yearMonth+""+(d<10?"0"+d:d);
	    vo = new ReservationVO(stay_no_n, stay_price_no_n, user_tel_v, reservationDate,
	            reservation_time_v, timeSplit[0], timeSplit[1], reservation_type_v, 0, pay_yn_v);
		result = dao.insertOne(vo);
	}
	
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
var result = "<%=result%>";
var msg = "등록실패";
if(result>0){
	msg ="등록성공";
}
alert(msg);
location.href = "reservation_list.jsp";
</script>
<title></title>
</head>
<body>

</body>
</html>