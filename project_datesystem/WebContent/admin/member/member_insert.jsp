<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>admin>member_>insert</title>
<!--  jQuery 기본 js파일 -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<!--  jQuery UI CSS파일  -->
<link rel="stylesheet" type="text/css" href="../../css/member/member_insert_style.css" />
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
<!--  jQuery UI 라이브러리 js파일 -->
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- 다음 주소 라이브러리 -->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$.datepicker.regional['ko'] = {
		closeText : '닫기',
		prevText : '이전달',
		nextText : '다음달',
		currentText : '오늘',
		monthNames : [ '1월', '2월', '3월', '4월', '5월', '6월',
				'7월', '8월', '9월', '10월', '11월', '12월' ],
		monthNamesShort : [ '1월', '2월', '3월', '4월', '5월',
				'6월', '7월', '8월', '9월', '10월', '11월', '12월' ],
		dayNames : [ '일', '월', '화', '수', '목', '금', '토' ],
		dayNamesShort : [ '일', '월', '화', '수', '목', '금', '토' ],
		dayNamesMin : [ '일', '월', '화', '수', '목', '금', '토' ],
		buttonImage : "/project_datesystem/images/btnCalendar.gif",
		changeYear : true,
		changeMonth : true,
		buttonImageOnly : true,
		showOn : "both",
		weekHeader : 'Wk',
		dateFormat : 'yy-mm-dd',
		firstDay : 0,
		isRTL : false,
		duration : 200,
		showAnim : 'show',
		showMonthAfterYear : false,
		//yearSuffix: '년',
		yearRange : '1920:2015',
		maxDate : 'today'
	};
	$.datepicker.setDefaults($.datepicker.regional['ko']);
	$("#member_bday_v").datepicker();

	$("#member_id_v").on("click", function() {
		var w = 240;
		var h = 150;
		LeftPosition = (screen.width - w) / 2;
		TopPosition = (screen.height - h) / 2;
		window.open("idDupl.jsp", "idDupl"
				,"left="+ LeftPosition + ",top=" + TopPosition + ",width=" + w + ",height=" + h
				+ ",status=no, scrollbars=no,resizable=no,location=no,titlebar=no");
	});

	$("#jibunBtn") .on("click", function() {
		var w = 890;
		var h = 380;
		LeftPosition = (screen.width - w) / 2;
		TopPosition = (screen.height - h) / 2;
		window.open("zipcode.jsp", "zipcode", "left=" + LeftPosition + ",top=" + TopPosition
				  + ",width=" + w + ",height=" + h
				  + ",status=no, scrollbars=no,resizable=no,location=no,titlebar=no");
	});

	$("#roadBtn").on("click", function() {
		new daum.Postcode({
			oncomplete : function(data) {
				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분입니다.
				// 예제를 참고하여 다양한 활용법을 확인해 보세요. http://postcode.map.daum.net/guide#usage
				$("#zipcode1_v").val(data.postcode1);
				$("#zipcode2_v").val(data.postcode2);
				$("#member_addr1_v").val(data.roadAddress);
				$("#member_addr2_v").focus();
			}
		}).open();
	});

});

function validate() {
	if ($("#member_id_v").val() == "") {
		alert("아이디를 입력해주세요");
		$("#member_id_v").focus();
		return false;
	}
	if ($("#member_pw_v").val() == "") {
		alert("암호를 입력해주세요");
		$("#member_pw_v").focus();
		return false;
	}
	if ($("#member_pw2_v").val() == "") {
		alert("암호를 입력해주세요");
		$("#member_pw2_v").focus();
		return false;
	}
	if ($("#member_pw_v").val() != $("#member_pw_v").val()) {
		alert("암호가 일치하지 않습니다");
		$("#member_pw_v").val("");
		$("#member_pw2_v").val("");
		$("#member_pw_v").focus();
		return false;
	}
	if ($("#member_nick_v").val() == "") {
		alert("닉네임을 입력해주세요");
		$("#member_nick_v").focus();
		return false;
	}
	if ($("#member_nm_v").val() == "") {
		alert("이름을 입력해주세요");
		$("#member_nm_v").focus();
		return false;
	}
	if ($("#member_bday_v").val() == "") {
		alert("생년월일을 입력해주세요");
		$("#member_bday_v").focus();
		return false;
	}
	if ($("#member_email_v").val() == "") {
		alert("이메일을 입력해주세요");
		$("#member_email_v").focus();
		return false;
	} else {
		if ($("#member_email_v").val().indexOf("@") == -1) {
			alert("이메일형식이 올바르지 않습니다");
			$("#member_email_v").val("");
			$("#member_email_v").focus();
			return false;
		}
	}
	if ($("#member_tel_v").val() == "") {
		alert("전화번호를 입력해주세요");
		$("#member_tel_v").focus();
		return false;
	}
	if ($("#member_cell_v").val() == "") {
		alert("휴대폰번호를 입력해주세요");
		$("#member_cell_v").focus();
		return false;
	}
	if ($("#zipcode1_v").val() == "" || $("#zipcode2_v").val() == "") {
		alert("주소를 입력해주세요");
		$("#zipcode1_v").focus();
		return false;
	}
	var addr1 = $("#member_addr1_v").val();
	var addr2 = $("#member_addr2_v").val();
	$("#member_addr2_v").val(addr1 + " " + addr2);

	if (confirm("등록하시겠습니까?")) {
		return true;
	}
}
</script>
<style type="text/css">
</style>
</head>
<body>
	<form action="./member_insertOk.jsp" onsubmit="return validate()">
		<div id="view_insert">
			<input type="hidden" name="member_addr_v" id="member_addr_v" />

			<div id="view_title">회 원 가 입 (관리자)</div>

			<div id="view_list">
				<ul>
					<li><label for="member_id_v" class="rec">아이디 : </label></li>
					<li><label for="member_pw_v" class="rec">암호 : </label></li>
					<li><label for="member_pw2_v" class="rec">암호 재입력 : </label></li>
					<li><label for="member_nick_v" class="rec">닉네임 : </label></li>
					<li><label for="member_nm_v" class="rec">이름 : </label></li>
					<li><label for="member_gender_v_M" class="rec">성별 : </label></li>
					<li><label for="member_bday_v" class="rec">생년월일 : </label></li>
					<li><label for="member_email_v" class="rec">이메일 : </label></li>
					<li><label for="member_tel_v" class="rec">전화번호 : </label></li>
					<li><label for="member_cell_v" class="rec">휴대폰번호 : </label></li>
					<li><label for="member_addr1_v" class="rec">주소 : </label></li>
					<li></li>
					<li><label for="member_addr2_v" class="rec">상세주소 : </label></li>
					<li><label for="member_type_v" class="rec">회원타입 : </label></li>

				</ul>
			</div>
			<div id="view_list_input">
				<ul>
					<li><input type="text" name="member_id_v" id="member_id_v"
						readonly /></li>
					<li><input type="text" name="member_pw_v" id="member_pw_v" /></li>
					<li><input type="text" name="member_pw2_v" id="member_pw2_v" /></li>
					<li><input type="text" name="member_nick_v" id="member_nick_v" /></li>
					<li><input type="text" name="member_nm_v" id="member_nm_v" /></li>
					<li><input type="radio" name="member_gender_v"
						id="member_gender_v_M" /> <label for="member_gender_v_M">남</label>
						<input type="radio" name="member_gender_v" id="member_gender_v_W" />
						<label for="member_gender_v_W">여</label></li>
					<li><input type="text" name="member_bday_v" id="member_bday_v"
						readonly /></li>
					<li><input type="text" name="member_email_v"
						id="member_email_v" /></li>
					<li><input type="text" name="member_tel_v" id="member_tel_v" /></li>
					<li><input type="text" name="member_cell_v" id="member_cell_v" /></li>
					<li><input type="text" id="zipcode1_v" readonly />-<input
						type="text" id="zipcode2_v" readonly /> <input type="button"
						value="지번" id="jibunBtn" /> <input type="button" value="도로명"
						id="roadBtn" /> <br /> <input type="text" name="member_addr1_v"
						id="member_addr1_v" readonly /></li>
					<li></li>
					<li><input type="text" name="member_addr2_v"
						id="member_addr2_v" /></li>
					<li><select id="member_type_v" name="member_type_v">
							<option value="N">일반회원
							<option value="S">관리자
					</select></li>
					<li><input type="submit" value="확인" class="btnPink" /></li>
				</ul>
			</div>
		</div>

	</form>

</body>
</html>