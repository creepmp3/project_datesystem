<%@page import="dao.FoodVO"%>
<%@page import="dao.FoodDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%
	int food_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("food_no_n"),"0"));
	String foodPath = "file:///C:/eclipse/web_workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/project_datesystem/upload/food/";

%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#listBtn").on("click", function(){
		location.href = "food_list.jsp";
	});
	
	$("#deleteBtn").on("click", function(){
		if ( confirm( "삭제하시겠습니까?" )) {
			$("#frm").attr("action", "food_deleteOk.jsp").submit();
		}
	});
	
	$("#modifyBtn").on("click", function() {
		$("#frm").attr("action", "food_modify.jsp").submit();
	});
});
</script>
<style type="text/css">
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<div class="view">
			<%
				FoodDAO dao = new FoodDAO();
				FoodVO vo = dao.selectOne(food_no_n);
				
				if(vo != null){
			%>
			<form id="frm">
				<input type="hidden" name="food_no_n" id="food_no_n" value="<%=food_no_n %>" />
				<table>	
					<colgroup>
						<col width="25%"/>
						<col width="75%"/>
					</colgroup>
					<tbody>
						<tr>
							<th>제목</th>
							<td><span class="titlev"><%=vo.getFood_title_v() %></span></td>
						</tr>
						<tr>
							<th>부제목</th>
							<td><%=vo.getFood_info_title_v() %></td>
						</tr>
						<tr>
							<th>내용</th>
							<td><%=vo.getFood_content_v() %></td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td><%=vo.getFood_tel_v() %></td>
						</tr>
						<tr>
							<th>OPEN TIME</th>
							<td><%=vo.getFood_open_time_v() %></td>
						</tr>
						<tr>
							<th>CLOSE TIME</th>
							<td><%=vo.getFood_close_time_v() %></td>
						</tr>
						<tr>
							<th>가격</th>
							<td><%=vo.getFood_price_v() %></td>
						</tr>
						<tr>
							<th>휴일</th>
							<td><%=vo.getFood_holiday_v() %></td> 
						</tr>
						<tr>
							<th>종류</th>
							<td><span class="food_type_v"><%=vo.getFood_type_v()%></span></td>
						</tr>
						<tr>
							<th>지역</th>
							<td><%=vo.getFood_loc_v() %></td>
						</tr>
						<tr>
							<th>등록일</th>
							<td><%=vo.getFood_reg_d()%></td>
						</tr>
						<tr>
							<th>이미지1</th>
							<td><img class="imgPath_1v" src="<%=vo.getFood_imgpath_1_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지2</th>
							<td><img src="<%=vo.getFood_imgpath_2_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지3</th>
							<td><img src="<%=vo.getFood_imgpath_3_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지4</th>
							<td><img src="<%=vo.getFood_imgpath_4_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지5</th>
							<td><img src="<%=vo.getFood_imgpath_5_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지6</th>
							<td><img src="<%=vo.getFood_imgpath_6_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지7</th>
							<td><img src="<%=vo.getFood_imgpath_7_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지8</th>
							<td><img src="<%=vo.getFood_imgpath_8_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
					</tbody>
				</table>
				<%
						}
				%>
				<div class="btnArea">
					<a href="#" class="btnPink" id="deleteBtn">삭제</a>
					<a href="#" class="btnPink" id="modifyBtn">수정</a>
					<a href="#" class="btnPink" id="listBtn">목록</a>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>