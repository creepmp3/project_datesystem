<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.oreilly.servlet.MultipartRequest" %>
<%@ page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@ page import="dao.TourDAO, dao.TourVO, util.StringUtil" %>
<%
	String uploadPath = request.getRealPath("/upload/tour");
	
	int maxSize = 1024*1024*10; // 10MB
	int result = 0;
	
		MultipartRequest multi = new MultipartRequest(request, uploadPath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
	
		String tour_title_v = multi.getParameter("tour_title_v");
		String tour_content_v = multi.getParameter("tour_content_v");
		String tour_tel_v = multi.getParameter("tour_tel_v");
		String tour_open_time_v = multi.getParameter("tour_open_time_v");
		String tour_close_time_v = multi.getParameter("tour_close_time_v");
		String tour_price_v = multi.getParameter("tour_price_v");
		String tour_holiday_v = multi.getParameter("tour_holiday_v");
		String tour_type_v = multi.getParameter("tour_type_v");
		String tour_loc_v = multi.getParameter("tour_loc_v");
		String tour_use_yn_c = multi.getParameter("tour_use_yn_c");
		String tour_imgpath_1_v = multi.getOriginalFileName("tour_imgpath_1_v");
		String tour_imgpath_2_v = multi.getOriginalFileName("tour_imgpath_2_v");
		String tour_imgpath_3_v = multi.getOriginalFileName("tour_imgpath_3_v");
		String tour_imgpath_4_v = multi.getOriginalFileName("tour_imgpath_4_v");
		String tour_imgpath_5_v = multi.getOriginalFileName("tour_imgpath_5_v");
		String tour_imgpath_6_v = multi.getOriginalFileName("tour_imgpath_6_v");
		String tour_imgpath_7_v = multi.getOriginalFileName("tour_imgpath_7_v");
		String tour_imgpath_8_v = multi.getOriginalFileName("tour_imgpath_8_v");
		
		TourDAO dao = new TourDAO();
		TourVO vo = new TourVO(tour_title_v, tour_content_v, tour_tel_v, tour_open_time_v,
	            tour_close_time_v, tour_price_v, tour_holiday_v, tour_type_v,
	            tour_loc_v, tour_use_yn_c, tour_imgpath_1_v, tour_imgpath_2_v,
	            tour_imgpath_3_v, tour_imgpath_4_v, tour_imgpath_5_v,
	            tour_imgpath_6_v, tour_imgpath_7_v, tour_imgpath_8_v);
		
		result = dao.insertOne(vo);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
var result = "<%=result%>";
var msg = "등록실패";
if(result>0){
	msg ="등록성공";
}
alert(msg);
location.href = "tour_list.jsp";
</script>
<title></title>
</head>
<body>

</body>
</html>