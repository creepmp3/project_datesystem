<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp"%>
<%@ page import="java.util.Calendar, dao.ReservationDAO, dao.ReservationVO, java.util.Date, java.text.SimpleDateFormat" %>
<%
	/* ******************** paging ******************** */
	int totalcount = 0;
	int pageCount = 0;
	int pageNo = Integer.parseInt(StringUtil.nvl(request.getParameter("pageNo"), "1"));
	int pageSize = 10;
	int startNum = ((pageNo - 1) * pageSize) + 1;
	int endNum = ((pageNo - 1) * pageSize) + pageSize;
	
	int searchStart = 0;
	int searchEnd = 0;
	searchStart = (pageNo-5 <= 0)?1:pageNo-5;
	searchEnd = (pageNo-5 <=0)?10:pageNo+5;

	String searchOption = StringUtil.nvl(request.getParameter("searchOption"), "");
	String searchKeyword = StringUtil.nvl(request.getParameter("searchKeyword"), "");
	/* ******************** paging ******************** */
	
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	String today = sdf.format(date);
	
	Calendar cal = Calendar.getInstance();
	int year = cal.get(Calendar.YEAR);
	int month = cal.get(Calendar.MONTH)+1;
	String startDate = "";
	String lastDate = "";
	
	String userDate = StringUtil.nvl(request.getParameter("userDate"), "");
	int userYear = 0;
	int userMonth = 0;
	if(!userDate.equals("")){
		userYear = Integer.parseInt(userDate.substring(0, 4));
		userMonth = Integer.parseInt(userDate.substring(5, 7));
		userMonth -= 1;
		cal.set(userYear, userMonth, 1);
		startDate = userYear+"-"+(userMonth<10?"0"+userMonth:userMonth)+"-"+"01";
		lastDate = cal.get(Calendar.YEAR)+"-"+(userMonth<10?"0"+userMonth:userMonth)+"-"+cal.getActualMaximum(Calendar.DATE);
		//lastDate = "2015-04-30";
	}else{
		
		startDate = year+"-"+(month<10?("0"+month):month)+"-01";
		lastDate = cal.get(Calendar.YEAR)+"-"+(month<10?("0"+month):month)+"-"+cal.getMaximum(Calendar.DAY_OF_MONTH);
	}
	System.out.println("startDate : "+startDate);
	ReservationDAO dao = new ReservationDAO();
	
	ArrayList<ReservationVO> list = dao.selectAll(startNum, endNum, startDate, lastDate, "calendar", searchKeyword);
	int size = list.size();
	System.out.println(PAGENAME);

%>


<link rel='stylesheet' href='/project_datesystem/js/fullcalendar-2.3.1/lib/cupertino/jquery-ui.min.css' />
<link href='/project_datesystem/js/fullcalendar-2.3.1/fullcalendar.css' rel='stylesheet' />
<link href='/project_datesystem/js/fullcalendar-2.3.1/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='/project_datesystem/js/fullcalendar-2.3.1/lib/moment.min.js'></script>
<script src='/project_datesystem/js/fullcalendar-2.3.1/fullcalendar.js'></script>
<script src='/project_datesystem/js/fullcalendar-2.3.1/lang-all.js'></script>
<script type="text/javascript">
$(function(){
	
	/* ********************** Calendar ********************** */
	var currentLangCode = 'ko';
	
	function renderCalendar() {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: "<%=today%>",
			lang: currentLangCode,
			buttonIcons: false, // show the prev/next text
			weekNumbers: true,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				<%
			    int i = 0;
				for(ReservationVO vo : list){
				    //out.println("vo : "+vo.getReservation_date_v());
				    //out.println("vot : "+vo.getReservation_time_v());
				    //out.println("t : "+today);
				    
				%> 
					
						{
							title: "<%=vo.getStay_price_nm_v()%> 예약",
							start: "<%=vo.getReservation_date_v()%>T<%=vo.getReservation_time_v()%>",
							detail: "<%=vo.getUser_tel_v()%> <%=StringUtil.nvl(vo.getPay_yn_v(), "N").equals("N")?"미결제":"결제완료"%>"
							//end: '2015-04-12T12:30:00'
						}
						
		 	<%  
 					i++;
				 	if(i!=vo.getTotalcount()){
						out.println(",");
					}
		 		}
			 	
				
		 	%> 
			]
		});
	}

	renderCalendar();
	
	$(".detail").hide();
	
	$(".fc-time").each(function(i, e){
		$(e).on("click", function(){
			$(this).next().next().toggle();
		});
	});

	
	$(".fc-title").each(function(i, e){
		$(e).on("click", function(){
			$(this).next().toggle();
		});
	});
	
	$(".resBtn").each(function(i, e){
		$(e).on("click", function(){
			var resDate = $(e).parent().attr("data-date");
			$("#resDate").val(resDate);
			$("#moveForm").attr("action", "reservation_insert.jsp").submit();
		});
	});
	
	
	/* ********************** Calendar ********************** */
	
	/* 
	$(".fc-time").each(function(i, e){
		$(e).on({
			"mouseover":function(){
				$(this).next().next().show();
			},
			"mouseout":function(){
				$(this).next().next().hide();
			}
		});
	});

	$(".fc-title").each(function(i, e){
		$(e).on({
			"mouseover":function(){
				$(this).next().show();
			},
			"mouseout":function(){
				$(this).next().hide();
			}
		});
	});
	 */
	
	
	$("#writeBtn").on("click", function(){
		location.href = "stay_insert.jsp";
	});

	$("a[name='titlev']").each(function(i, elem){
		$(this).on("click", function(){
			var no = $(this).parent().prev().text();
			$("#stay_no_n").val(no);
			$("#moveForm").attr("action", "stay_view.jsp").submit();
		});
	});
	
	$("#reservationBtn").on("click", function(){
		location.href = "./reservation_insert.jsp";
	});
	
});
</script>

</head>
<body>
<form id="moveForm">
	<input type="hidden" name="stay_no_n" id="stay_no_n" />
	<input type="hidden" name="resDate" id="resDate" />
</form>
	<div class="list">
	<h2>관리자페이지</h2>
		<div id='calendar'></div>
		<div class="btnArea">
			<a href="#" class="btn" id="deleteBtn">삭제</a>
			<a href="#" class="btn" id="reservationBtn">예약하기</a>
			<a href="#" class="btn" id="writeBtn">등록하기</a>
		</div>
	</div>
<%@ include file="/common/sub_footer.jsp" %>