<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="db.MakeConnection, java.sql.Connection, java.sql.PreparedStatement, java.sql.ResultSet, util.StringUtil"%>
<%
	request.setCharacterEncoding("UTF-8");

	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	int stay_price_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_price_no_n"), "0"));
	String date = StringUtil.nvl(request.getParameter("date"), "");
	String time = StringUtil.nvl(request.getParameter("time"), "");
	
	Connection conn = MakeConnection.getInstance().getConnection();
	StringBuffer sql = new StringBuffer();
	sql.append("\n SELECT count(*) count                            ");
	sql.append("\n 	 FROM t_reservation r, t_stay s, t_stay_price p ");
	sql.append("\n  WHERE r.stay_no_n = p.stay_no_n                 ");
	sql.append("\n    AND r.stay_price_no_n = p.stay_price_no_n     ");
	sql.append("\n    AND r.reservation_use_yn_v = 'Y'              ");
	//sql.append("\n    AND r.pay_yn_v = 'Y'                          ");
	sql.append("\n    AND p.stay_no_n = ?                           ");
	sql.append("\n    AND p.stay_price_no_n = ?                     "); 
	sql.append("\n    AND r.reservation_date_v = ?                  ");
	sql.append("\n    AND r.reservation_time_v = ?                  ");

	PreparedStatement pstmt = conn.prepareStatement(sql.toString());
	pstmt.setInt(1, stay_no_n);
	pstmt.setInt(2, stay_price_no_n);
	pstmt.setString(3, date);
	pstmt.setString(4, time);
	ResultSet rs = pstmt.executeQuery();

	rs.next();
	int count = rs.getInt("count");
	System.out.print("count:"+count);	
%>
{"count":"<%=count%>"}