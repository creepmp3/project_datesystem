<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.ReservationDAO, dao.ReservationVO" %>

<%
	/* ******************** paging ******************** */
	int totalcount = 0;
	int pageCount = 0;
	int pageNo = Integer.parseInt(StringUtil.nvl(request.getParameter("pageNo"), "1"));
	int pageSize = 10;
	int startNum = ((pageNo - 1) * pageSize) + 1;
	int endNum = ((pageNo - 1) * pageSize) + pageSize;
	
	int searchStart = 0;
	int searchEnd = 0;
	searchStart = (pageNo-5 <= 0)?1:pageNo-5;
	searchEnd = (pageNo-5 <=0)?10:pageNo+5;

	String searchOption = StringUtil.nvl(request.getParameter("searchOption"), "");
	String searchKeyword = StringUtil.nvl(request.getParameter("searchKeyword"), "");
	/* ******************** paging ******************** */
	
	ReservationDAO dao = new ReservationDAO();
	ArrayList<ReservationVO> list = dao.selectAll(startNum, endNum, "", "", searchOption, searchKeyword);
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!--  jQuery 기본 js파일 -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>

<link rel="stylesheet" type="text/css" href="../../css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/menu_content.css" />
<link rel="stylesheet" href="../../css/content_css/tour_content_style.css" />
<link rel="stylesheet" href="../css/content_css/tour_content_style.css" />
<link rel="stylesheet" href="../../css/main/tour_style.css" />
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />

<script type="text/javascript">
$(function(){
	$("#writeBtn").on("click", function(){
		location.href = "stay_insert.jsp";
	});

	$("a[name='titlev']").each(function(i, elem){
		$(this).on("click", function(){
			var no = $(this).parent().prev().text();
			$("#stay_no_n").val(no);
			$("#moveForm").attr("action", "stay_view.jsp").submit();
		});
	});
	
	$("#reservationBtn").on("click", function(){
		location.href = "./reservation_insert.jsp";
	});

	$("#calendarBtn").on("click", function(){
		location.href = "./reservation_calendar_list.jsp";
	});
	
});
</script>
</head>
<style type="text/css">
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<form id="moveForm">
			<input type="hidden" name="stay_no_n" id="stay_no_n" />
		</form>
		<div class="list">
			<h2>관리자페이지</h2>
			<div class="searchWrap">
				<form action="<%=PAGENAME%>">
					<select name="searchOption">
						<option value="titlev" <%=searchOption.equals("titlev")? "selected='selected'":"" %>>이름</option>
						<option value="loc" <%=searchOption.equals("loc")? "selected='selected'":"" %>>지역</option>
						<option value="type" <%=searchOption.equals("type")? "selected='selected'":"" %>>타입</option>
						<option value="holiday" <%=searchOption.equals("holiday")? "selected='selected'":"" %>>휴일</option>
						<option value="tel" <%=searchOption.equals("tel")? "selected='selected'":"" %>>전화번호</option>
					</select>
					<input type="text" name="searchKeyword" value="<%=searchKeyword%>" />
					<input type="submit" value="검색" />
				</form>
			</div>
			<table>
				<thead>
					<tr>
						<th><input type="checkbox" id="chkAll"/></th>
						<th>번호</th>
						<th>전화번호</th>
						<th>이름</th>
						<th>예약날짜</th>
						<th>예약시간</th>
						<th>결제상태</th>
						<th>등록일</th>
					</tr>
				</thead>
				<tbody>
				<%
					
					if(list.size()>0){
						for(ReservationVO vo : list){
							totalcount = vo.getTotalcount();
							
							if(totalcount%pageSize==0){
								pageCount = totalcount/pageSize;
							}else{
								pageCount = totalcount/pageSize+1;
							}
				%>
						<tr>
							<td><input type="checkbox" name="chk"/></td>
							<td><%=vo.getReservation_no_n()%></td>
							<td><a href="reservation_view.jsp?reservation_no_n=<%=vo.getReservation_no_n() %>" name="titlev"><%=vo.getUser_tel_v()%></a></td>
							<td><%=vo.getStay_title_v() %></td>
							<td><%=vo.getReservation_date_v() %></td>
							<td><%=vo.getReservation_time_v() %></td>
							<td><%=StringUtil.nvl(vo.getPay_yn_v(), "N").equals("N")?"미결제":"결제완료" %></td>
							<td><%=vo.getReservation_regd_d() %></td>
						</tr>
				<%
						}
					}else{
				%>
					<tr><td colspan="8">데이터가 없습니다</td></tr>
				<%
					} 
				%>
				</tbody>
			</table>
			<div class="paging">
				<div class="pagenum">
					<%
					for(int i=searchStart; i<=pageCount; i++){
						if(i<0){
							continue;
						}else if(i>searchEnd || totalcount<10){
							break;
						}else if(i==pageNo){
							out.println("<strong>"+i+"</strong>");
						}else{
							out.println("<a class='pagingBtn' href='reservation_list.jsp?pageNo="+i+"&searchKeyword="+searchKeyword+" '>"+i+"</a>");
						}
					}
					out.println("<span>("+pageNo+"/"+pageCount+" 총" +totalcount+"개)</span>");
					%>
				</div>
				<div class="btnArea">
					<a href="#" class="btnPink" id="deleteBtn">삭제</a>
					<a href="#" class="btnPink" id="calendarBtn">달력보기</a>
					<!-- <a href="#" class="btn" id="reservationBtn">예약하기</a> -->
					<!-- <a href="#" class="btnPink" id="writeBtn">등록하기</a> -->
				</div>
			</div>
			
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>