<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.ReservationDAO, dao.ReservationVO, util.StringUtil" %>
<%
	int reservation_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("reservation_no_n"),"0"));

%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#listBtn").on("click", function(){
		location.href = "reservation_list.jsp";
	});
	
	$("#deleteBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			//$("#frm").attr("action", "stay_deleteOk.jsp").submit();
		}
	});
	
	$("#priceAddBtn").on("click", function(){
		//$("#frm").attr("action", "stay_price_insert.jsp").submit();
	});
	
	$("#modifyBtn").on("click", function(){
		//$("#frm").attr("action", "stay_modify.jsp").submit();
	});
		
	
});
</script>
</head>
<style type="text/css">
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<div class="view">
		<h2>관리자페이지</h2>
			<%
				ReservationDAO dao = new ReservationDAO();
				ReservationVO vo = dao.selectOne(reservation_no_n);
				String dayType= "";
				if(vo != null){
				    if(vo.getReservation_type_v().equals("dw")){
				        dayType = "대실 평일";
				    }else if(vo.getReservation_type_v().equals("dh")){
				        dayType = "대실 주말";
				    }else if(vo.getReservation_type_v().equals("sw")){
				        dayType = "숙박 평일";
				    }else if(vo.getReservation_type_v().equals("sh")){
				        dayType = "숙박 주말";
				    }
			%>
			<form id="frm">
				<input type="hidden" name="reservation_no_n" id="reservation_no_n" value="<%=reservation_no_n %>" />
				<input type="hidden" name="stay_no_n" id="stay_no_n" value="<%=vo.getStay_no_n() %>" />
				<input type="hidden" name="stay_price_no_n" id="stay_price_no_n" value="<%=vo.getStay_price_no_n() %>" />
				<table>
					<tbody>
						<tr>
							<th>업소명</th>
							<td><span class="titlev"><%=vo.getStay_title_v() %></span></td>
						</tr>
						<tr>
							<th>숙박등급</th>
							<td><%=vo.getStay_price_nm_v() %></td>
						</tr>
						<tr>
							<th>숙박구분</th>
							<td><%=dayType %></td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td><%=vo.getUser_tel_v()%></td>
						</tr>
						<tr>
							<th>예약날짜</th>
							<td><%=vo.getReservation_date_v() %></td>
						</tr>
						<tr>
							<th>예약시간</th>
							<td><%=vo.getReservation_time_v() %></td>
						</tr>
						<tr>
							<th>가격</th>
							<td><%=vo.getStay_price_n() %></td>
						</tr>
						<tr>
							<th>승인</th>
							<td><%=vo.getConfirm_yn_v() %></td> 
						</tr>
						<tr>
							<th>등록일</th>
							<td><%=vo.getReservation_regd_d()%></td>
						</tr>
					</tbody>
				</table>
				<%
						}
				%>
				<div class="btnArea">
					<a href="#" class="btnPink" id="deleteBtn">삭제</a>
					<a href="#" class="btnPink" id="modifyBtn">수정</a>
					<a href="#" class="btnPink" id="listBtn">목록</a>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>