<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(document).ready(function(){
	
	
});

function validate(){
	if($("#stay_price_nm_v").val()==""){
		alert("구분명을 입력해주세요");
		$("#stay_price_nm_v").focus();
		return false;
	}
	if($("#stay_d_w_v").val()==""){
		alert("대실 평일을 입력해주세요");
		$("#stay_d_w_v").focus();
		return false;
	}
	if($("#stay_d_h_v").val()==""){
		alert("대실 주말을 입력해주세요");
		$("#stay_d_h_v").focus();
		return false;
	}
	if($("#stay_s_w_v").val()==""){
		alert("숙박 평일을 입력해주세요");
		$("#stay_s_w_v").focus();
		return false;
	}
	if($("#stay_s_h_v").val()==""){
		alert("숙박 주말을 입력해주세요");
		$("#stay_s_h_v").focus();
		return false;
	}
	if(confirm("등록하시겠습니까?")){
		return true;
	}
}
</script>
	
<div id="tableWrap">
	<h2>관리자페이지</h2>
	<form action="./stay_price_insertOk.jsp" onsubmit="return validate()">
		<input type="hidden" name="stay_no_n" value="<%=stay_no_n%>" />
		<table class="tableView">
			<tr>
				<th><label for="stay_price_nm_v">구분명</label></th>
				<td><input type="text" name="stay_price_nm_v" id="stay_price_nm_v" style="width:99%" /></td>
			</tr>
			<tr>
				<th><label for="stay_d_w_v">대실 평일 가격</label></th>
				<td><input type="text" name="stay_d_w_v" id="stay_d_w_v"/></td>
			</tr>
			<tr>
				<th><label for="stay_d_h_v">대실 주말 가격</label></th>
				<td><input type="text" name="stay_d_h_v" id="stay_d_h_v"/></td>
			</tr>
			<tr>
				<th><label for="stay_s_w_v">숙박 평일 가격</label></th>
				<td><input type="text" name="stay_s_w_v" id="stay_s_w_v"/></td>
			</tr>
			<tr>
				<th><label for="stay_s_h_v">숙박 주말 가격</label></th>
				<td><input type="text" name="stay_s_h_v" id="stay_s_h_v"/></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="확인" class="btn"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<%@ include file="/common/sub_footer.jsp" %>