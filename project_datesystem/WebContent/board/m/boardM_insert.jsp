<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/community_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/main/weather.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="/project_datesystem/js/main/movie_js_style.js" /></script>
<title>Insert title here</title>
<script type="text/javascript">
$(function(){
	$("#insertBtn").on("click", function(){
		location.href = "./boardM_insert.jsp";
	});
});
</script>
</head>
<body>
	<div id = "main">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		<div id = "sub_menu">
			<div id = "profile">
				xx님 <br/>
				상태 상태 <br/>
				뭐야 뭐야 <br/>
			</div>
			<ul>
				<li>사이드 메뉴1</li>
				<li>사이드 메뉴2</li>
				<li>사이드 메뉴3</li>
				<li>사이드 메뉴4</li>
				<li>사이드 메뉴5</li>
				<li>사이드 메뉴6</li>
				<li>사이드 메뉴7</li>
			</ul>
		</div>
		<div id = "sign_area">광고 영역</div>
		<div id = "board_content">
			<div style="border:0px solid black;width:735px;height:655px;position:absolute;top:-227px">
				<form id="listForm">
					<table style="border:1px solid black;width:735px;height:auto">
						<colgroup>
							<col width="30%"/>
							<col width="70%"/>
						</colgroup>
						<tr>
							<th>제목</th>
							<td><input type="text" name="title_v" id="title_v" /></td>
						</tr>
						<tr>
							<th>내용</th>
							<td>
								<textarea name="content_v" id="content_v" cols="30" rows="10"></textarea>
							</td>
						</tr>
					</table>
				</form>
				<div class="btnArea">
					<a href="#" id="insertBtn">글쓰기</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>