<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>D-day</title>
<script type="text/javascript">
function Dday(y, m, d, hh, mm){
	
	var endtxt = y+"년 "+m+"월 "+d+"일 "+hh+"시 "+mm+"분 까지<br/><br/>";
	
	var today = new Date();
	var todayY = today.getYear();
	if(todayY < 1000) todayY += 1900;
	var monthArray = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	var todayM = today.getMonth();
	var todayD = today.getDate();
	var todayHour = today.getHours();
	var todayMin = today.getMinutes();
	var todaySec = today.getSeconds();
	
	var now = monthArray[todayM] + " " + todayD + ", " + todayY + " " + todayHour + ":" + todayMin + ":" + todaySec;
	var dday = monthArray[m-1] + " " + d + ", " + y + " " + hh + ":" + mm;
	
	dd = Date.parse(dday) - Date.parse(now);
	dy = Math.floor((dd / 1000/60/60/24)/365);
	dm = Math.floor((dd / 1000/60/60/24)/30);
	
	dday = Math.floor(dd / (60*60*1000*24) * 1);
	dhour = Math.floor((dd % (60*60*1000*24)) / (60*60*1000) * 1);
	dmin = Math.floor(((dd % (60*60*1000*24)) % (60*60*1000)) / (60*1000) * 1);
	dsec = Math.floor((((dd % (60*60*1000*24)) % (60*60*1000)) % (60*1000)) / 1000 * 1);
	
	var txt = "";
	if(dday==0 && dhour==0 && dmin==0 && dsec==1){
		txt = "종료";
	}else if(dy==0 && dy!=0){
		txt = endtxt + dm + "개월, " + dday + "일, " + dhour + "시간, " + dmin + "분, " + dsec + "초 남았습니다.";
	}else if(dy==0 && dm==0 && dday!=0){
		txt = endtxt + dday + "일, " + dhour + "시간, " + dmin + "분, " + dsec + "초 남았습니다.";
	}else if(dy==0 && dm==0 && dday==0 && dhour!=0){
		txt = endtxt + dhour + "시간, " + dmin + "분, " + dsec + "초 남았습니다.";
	}else if(dy==0 && dm==0 && dday==0 && dhour==0 && dmin!=0){
		txt = endtxt + dmin +"분, " + dsec + "초 남았습니다.";
	}else if(dy==0 && dm==0 && dday==0 && dhour==0 && dmin==0 && dsec!=0){
		txt = endtxt + dsec + "초 남았습니다.";
	}else if (dy!=0){
		txt = endtxt + dy+"년, "+dm+"개월, "+dday+"일, "+dhour+"시간, "+dmin+"분, "+dsec+"초 남았습니다.";
	}
	document.getElementById("canvas").innerHTML = txt;
	
}

setInterval("Dday(2015,5,1,09,30)",1000);

</script>
<body>
	<div id="canvas"></div>
</body>
</html>