<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.MemberDAO, dao.MemberVO, util.StringUtil" %>
<%
	String id = StringUtil.nvl(request.getParameter("member_id_v"), "");
	String pw = StringUtil.nvl(request.getParameter("member_pw_v"), "");
	MemberDAO dao = new MemberDAO();
	MemberVO vo = dao.login(id, pw);
	
	int result = vo.getCount();
	String type = vo.getMember_type_v();
	
	if(result==2){
		session.setAttribute("member_id_v", id);
		session.setAttribute("member_pw_v", pw);
		session.setAttribute("member_type_v", type);
	}
	response.sendRedirect("login.jsp");
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title></title>
</head>
<body>
</body>
</html>