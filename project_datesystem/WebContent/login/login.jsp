<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../common/sub_header.jsp" %>
<%@ page import="util.StringUtil" %> 
<link rel = "stylesheet"  href = "../css/login/login_style.css">
<%
	String id = StringUtil.nvl(request.getParameter("member_id_v"), "");
	String pw = StringUtil.nvl(request.getParameter("member_pw_v"), "");
	int loginResult = 0;

	String retUrl = StringUtil.nvl(request.getParameter("retUrl"), "");
	
	if (!id.equals("")) {
		MemberDAO dao = new MemberDAO();
		MemberVO vo = dao.login(id, pw);
	
		loginResult = vo.getCount();
		int member_no_n = vo.getMember_no_n();
		String type = vo.getMember_type_v();
		String name = vo.getMember_nick_v();
		String gender = vo.getMember_gender_v();
		String member_cell_v = vo.getMember_cell_v();

		if (loginResult == 2) {
			session.setAttribute("member_no_n", member_no_n);
			session.setAttribute("member_id_v", id);
			session.setAttribute("member_pw_v", pw);
			session.setAttribute("member_nick_v", name);
			session.setAttribute("member_gender_v", gender);
			session.setAttribute("member_type_v", type);
			session.setAttribute("member_cell_v", member_cell_v);

			
			if(!retUrl.equals("")){
		        response.sendRedirect(retUrl);
			}else{
		        response.sendRedirect("/project_datesystem/contents/main_content.jsp");
			}
		}
	}
%>
<script type="text/javascript">
function validate(){
	if($("#member_id_v").val()==""){
		alert("아이디를 입력해주세요");
		$("#member_id_v").focus();
		return false;
	}
	if($("#member_pw_v").val()==""){
		alert("비밀번호를 입력해주세요");
		$("#member_pw_v").focus();
		return false;
	}
	return true;
}
</script>
		<%
		if(!session_member_id_v.equals("")){
		    
		}else{
		%>
			<div id="loginTable">
				<form action="<%=URI %>" onsubmit="return validate()">
					<input type="hidden" name="retUrl" value="<%=retUrl %>" />
							
							<div id = "login_logo">
								환영합니다 고갱님~
								<div id = "page_move">
									<a href = "/project_datesystem/contents/main_content.jsp">메인 홈</a> | 고객센터
								</div>
							</div>
							<div id = "login_area">
							<input type="text" name="member_id_v" id="member_id_v" placeholder="아이디"/>
							<input type="image" src="../images/login/btn_login.gif" value="로그인" id = "button_img"/>
							<input type="text" name="member_pw_v" id="member_pw_v" placeholder="비밀번호" />
							<div id = "login_member">
								<a href="###">아이디,비밀번호를 잊으셨어요?</a> | <a href="/project_datesystem/user/member/member_insert.jsp">회원가입</a>
							</div>
							</div>
							<div id = "page_image">
							<img src = "../images/logo/loging_image.jpg" id = "sub_image">
							</div>
							
							<div id = "page_list">
							</div>
					
				
				</form>
			</div>
		<%
			}
		%>

			