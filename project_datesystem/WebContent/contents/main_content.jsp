<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>


<link rel="stylesheet" type="text/css" href="../css/content_css/main_contents_style.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

</head>
<body>
	<div id = "main">
		<div id = "top">
			<jsp:include page="content_menu.jsp"></jsp:include>
		</div>
		<div id = "content">
			<jsp:include page="sub_content.jsp"></jsp:include>
		</div>
	<!-- 	<div id = "footer">
			footer
		</div> -->
	</div>
</body>
</html>