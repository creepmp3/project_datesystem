<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page
	import="dao.MemberDAO, dao.MemberVO, util.StringUtil, java.util.ArrayList"%>
<%

	String PAGENAME = request.getRequestURL().toString();
	String retUrl = StringUtil.nvl(request.getParameter("retUrl"), "");

	String session_member_id_v = StringUtil.nvl(
			(String) session.getAttribute("member_id_v"), "");
	String session_member_pw_v = StringUtil.nvl(
			(String) session.getAttribute("member_pw_v"), "");
	String session_member_nick_v = StringUtil.nvl(
			(String) session.getAttribute("member_nick_v"), "");
	String session_member_type_v = StringUtil.nvl(
			(String) session.getAttribute("member_type_v"), "");

	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="/project_datesystem/css/content_css/menu_content.css" />
<script type="text/javascript" src="/project_datesystem/js/main/weather.js"></script>
<link rel="stylesheet" href="/project_datesystem/css/main/weather.css" />
<title>Insert title here</title>
<script type="text/javascript">
$(document).ready(function() {

	/* ********************** 날씨 ********************** */
	expires = new Date();
	expires.setTime(expires.getTime()
			+ (1000 * 3600 * 24 * 365));
	set_cookie('cityid', 1835848, expires);
	$.getJSON("http://api.openweathermap.org/data/2.5/forecast/daily?callback=?&id=1835848&units=metric&cnt=14",
				showForecastDaily).error(errorHandler);
	/* ********************** 날씨 ********************** */
	console.log("1" + expires);
	/* 이미지슬라이드 */
	if("<%=PAGENAME%>".indexOf('main')>-1){
		//imageSilide(2000);
	}

});

function logout() {
	if (confirm("로그아웃 하시겠습니까?")) {
		location.href = "/project_datesystem/login/logout.jsp";
	}
}
</script>
<style type="text/css">
#member {
	text-shadow: 1px 1px 3px #909090;
}
#member a{
	font-weight:bold;
}
</style>
</head>
<body>
	<div id="logo"></div>
	<div id="member">
		<%
			if (session_member_id_v.equals("")) {
		%>
		<a href="/project_datesystem/login/login.jsp?retUrl=<%=PAGENAME%>">로그인</a> | <a
			href="/project_datesystem/user/member/member_insert.jsp">회원가입</a>
		<%
			} else {
		%>
		<a href="#"><%=session_member_nick_v%></a>님 | <a href="#"
			id="logoutBtn" onclick="logout()">로그아웃</a>
		<%
			}
		%>
		<%
			if (session_member_type_v.equals("S")) {
		%>
		| <a href="/project_datesystem/admin/admin_list.jsp">Admin</a><br />
		<br />
		<%
			}
		%>
	</div>
	<div id="menu">
		<ul>
			<li><a href="/project_datesystem/contents/main_content.jsp">홈</a></li>
			<li><a href="/project_datesystem/user/food/food_list.jsp">먹으러갈래?</a></li>
			<li><a href="/project_datesystem/user/tour/tour_list.jsp">놀러갈래?</a></li>
			<li><a href="/project_datesystem/contents/movie.jsp">영화볼래?</a></li>
			<li><a href="/project_datesystem/user/stay/stay_list.jsp">자러갈래?</a></li>
			<li><a href="/project_datesystem/user/board/community.jsp">이야기하자!</a></li>
			<!-- <li>이거바꿔줘!</li> -->
		</ul>


		<div id="daily_list"></div>

	</div>

