<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="../css/content_css/movie_style.css" />
<link rel="stylesheet" type="text/css"
	href="../css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="../css/main/weather.css" />

<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript"
	src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="../js/main/movie_js_style.js" /></script>
<title>Insert title here</title>
</head>
<body>
	<div id="main">
		<jsp:include page="content_menu.jsp"></jsp:include>
		<div id="movie_content">
			<div id="title_movie">
				<div id="title">추천 개봉작</div>
				<div id="movie_title_image">
					<ul>
						<li id="movie_image_1"></li>
						<li id="movie_image_2"></li>
						<li id="movie_image_3"></li>
						<li id="movie_image_4"></li>
						<li id="movie_image_5"></li>
					</ul>
				</div>
			</div>

			<div id="show_movie">
				<div id="sub_title">영화소개</div>
				<div id="sub_movie">
					<div id="image1_1"></div><br/>
					<div align="center">
					<a target="_blank" class="link_addr" href = " http://ticket2.movie.daum.net/yes24/default.aspx?m_id=M000049037 ">
					<input type = "button" size="20" name = "" id="btn_2" value = "예매하러가기▶"/>
					</a>
					</div>
				</div>
				<div id="detail_movie">
					<div id="detail_movie_title">
						<ul>
							<li id="desc1_1"></li>
							<li id="btn"><input type = "button" name = "" id="btn_1" value = "상영중▶"/></li>
							<li id="desc1_2">감독 : </li>
							<li id="desc1_3">장르 : </li>
							<li id="desc1_4">국가 : </li>
							<li id="desc1_5">개요 : </li>
							<li id="desc1_6">출연 배우 : </li>
						</ul>
					</div>
					<div id = "story_area">
						<div id = "story_title">주요 정보</div>
						<div id = "story1"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>