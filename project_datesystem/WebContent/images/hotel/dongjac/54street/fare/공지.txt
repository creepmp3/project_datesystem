업체 공지사항
전객실 커플 PC, PC방 사양의 컴퓨터와 무선 와이파이
(기가랜 구축, 노하드 시스템으로 PC방과 동일한 게임 자동 패치)
서울대입구역 54번가 호텔 9월 그랜드 오픈
최고의 시설 최상의 서비스로 회원님을 모시겠습니다.
회원혜택은 제휴점 내규에 따라 적용됩니다.
회원혜택은 입실시 프론트에서 말씀하셔야 적용가능(입실 후 불가능)
객실 안내
전객실 손님들의 편안한 잠자리를 위해 라텍스 침대&거위털 침구류 사용
전객실 50인치&55인치 최신스마트 TV가 설치되어 있습니다.
전객실 최신영화&TV다시보기 기능이 설치되어 있습니다.
전객실 최고급 에어풀 스파설치
전객실 공조 시스템(급기, 배기, 환기장치 설치)
전객실 최고급 일체형 비데 설치
전객실 커플 PC, PC방 사양의 컴퓨터와 무선 와이파이(기가랜 구축, 노하드 시스템으로 PC방과 동일한 게임 자동 패치)
예약 안내
대실 : 예약불가 (방문입실)
숙박 : 주말 예약가능(디럭스, 프리미엄, VIP, VVIP)
계좌 이체한 분들만 적용(22시 이후 입실가능)
계좌번호 : 404601-01-152734 국민은행, 변광규(54번가)
예약 환불규정
2일전 : 100% 환불
1일전 : 30% 환불
당일 : 환불 불가
서울대입구의 54번가로 여러분을 초대합니다.
2014년 9월 , 서울 최고의 호텔이 오픈합니다.
최고급 대리석으로 완벽한 모던함을 추구합니다.
세련된 도시감성으로 여러분을 초대합니다.