package kr.co.hbilab.app;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

//20150527
public class TestMain {
    public static void main(String[] args) {
       ApplicationContext ctx = new GenericXmlApplicationContext("app.xml");
       Dao dao = ctx.getBean("dao", Dao.class);
       
       List<DeptDTO> list = dao.selectAll();
       
       System.out.println("부서번호\t\t부서명\t\t부서위치");
       for(DeptDTO dto : list){
           System.out.print(dto.getDeptno()+"\t\t");
           System.out.print(dto.getDname()+"\t");
           System.out.print(dto.getLoc()+"\n");
       }
       System.out.println("-------------------------------------------");
       
       DeptDTO dto = dao.selectOne(10);
       System.out.println("부서번호 : " + dto.getDeptno());
       System.out.println("부서명 : " + dto.getDname());
       System.out.println("부서위치 : " + dto.getLoc());
    }
}
