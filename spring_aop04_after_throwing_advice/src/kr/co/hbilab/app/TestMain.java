package kr.co.hbilab.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * 2015. 5. 28.
 * @author user
 *
 */
public class TestMain {
    public static void main(String[] args) {
        ApplicationContext ctx = new GenericXmlApplicationContext("app.xml");
        Message m = ctx.getBean("proxyBean", Message.class);
        m.printMsg();
        try{
            m.printThrowWxception();
        }catch(Exception e){
            
        }
    }
}
