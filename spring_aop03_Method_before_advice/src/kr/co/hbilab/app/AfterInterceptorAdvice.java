package kr.co.hbilab.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.aop.AfterReturningAdvice;

public class AfterInterceptorAdvice implements AfterReturningAdvice{

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args,
            Object target) throws Throwable {
        
        String filename = new SimpleDateFormat("yyyyMMdd").format(new Date());
        File f = new File("c:/log/log"+filename+".txt");

        String s = null;
        String content = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
        while((s = br.readLine())!=null){
            content += s +"\r\n";
        }
        
        PrintWriter pw = new PrintWriter(f);
        String now = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        pw.write(content+"[Log] : " + target+ " ("+now+")");

        f.createNewFile();
        pw.flush();
        pw.close();
    }
    
}
